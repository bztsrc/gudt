Grand Unified Device Tree
=========================

In short [GUDT](https://gitlab.com/bztsrc/gudt) (pronounced like Earl Gray, as in a "good tea") provides a platform-independent and
standardized way of describing devices in a computer. It is similar to UEFI's [ACPI bytecode](https://uefi.org/specifications)
(AML) or the [FDT](https://www.devicetree.org/specifications/) (also referred to as DTB); but it is *MUCH* simpler and more
powerful than those, has no security concerns like AML and there's no need for generated interface bindings like with FDT. Unlike
AML and just like FDT you can also use it to pass a boot time configuration.

So GUDT is truly universal and easy to use, and it's Free and Open Source.

The [gudt.h](gudt.h) is a single header ANSI C library for applications that want to consume GUDT blobs (does not need libc and
does not allocate memory either, so works on bare metal too). This header is very very small, because the format is really easy
to use, yet featureful and powerful.

The [gudt.ids](https://gitlab.com/bztsrc/gudt/raw/main/gudt.ids) file stores the [Universal Device Type](docs#universal-device-type)
codes used in the GUDT blobs. This registry is freely available to everyone and backwards compatible with the already existing
similar databases. The blob contains the exact device driver's name, so these type codes are just optional to help indetify the
kind and maker of the device.

The [gudt](src) utility is a dependency-free command line tool to create, compress, uncompress, dump and convert from DSDT/SSDT
(.aml), FDT (.dtb) or JSON into this format. The human readable textual source is a simple JSON string in UTF-8 encoding, which
can be edited by an armada of already existing 3rd party tools, or by the `gudtgui` application. See [examples](examples).

<img src="https://gitlab.com/bztsrc/gudt/raw/main/examples/gudtgui_aml.png">

File Format
-----------

- file extension: .gud
- mime type: application/grand-unified-device-tree

To be easily identifiable, starts with the `GUDT` magic bytes. See the [Grand Unified Device Tree specification](docs) for more
information.

Roads Not Taken
---------------

A few thoughts behind the design decisions of GUDT.

- no bytecode. ACPI requires a virtual machine to interpret device definitions. This is totally insane, because hardware descriptor
  device trees are static for a particular machine. They cannot change in run-time as removeable devices are not in the list, just
  their controllers. As AML has demonstrated with many many zero-day exploits, a bytecode imposes a huge security risk on end-users,
  and with all that buggy and faulty DSDT tables in the wild it is obvious that hardware manufacturers are having really hard time
  using a bytecode too.

- no nested nodes. Both AML and FDT use a nested node structure (so despite its name, Flattended Device Tree actually isn't
  flattened at all). The problem with this is, that you can only parse this sequentially; you cannot just simply get the Nth node.
  Adding and removing a node is a very complicated task (take a look at *iasl* and *dtc* source code and see how complex they are).

- no variable length nodes. Same problem as with nested nodes, only sequential parsing is possible with these, and adding and
  removing nodes is problematic. GUDT uses fixed sized nodes in a one dimensional array; that can be traversed and modified easily
  just as-is. It might seem at first that fixed sized records waste memory and storage space, but just compare the same device tree
  in AML, FDT and GUDT formats, and you'll see that GUDT is actually the smallest of them all (even without compression).

- no user-defined property keys. Although flexibile keys sounds great at first, but FDT has demonstrated that it is a major PITA in
  practice. A device driver can't just parse a DTB, yet another tool is needed to generate source code bindings for that particular
  set of keys. And just take a look at the existing .dtb files: each and every manufacturer has defined their own, incompatible
  properties to describe their otherwise fully compatible GPIO chips. A well-defined, unambiguous, standardized set of keys are
  highly desireable, so that one could once and for all forget about that ugly source code binding trickery.

- no fixed property values. The other end of extremity, AML mandates a strange way to encode strings which results in limited and
  cryptic, hard to identify device and driver names. FDT got this right (although it does not put these strings in the string table
  for some reason unknown to me, just the property keys). Unlike property keys, property values must be flexible to suit all
  possible existing and future hardware out there, so the format must not limit the values.

- no paywalled registries. FDT doesn't even bother to create a list of its device types, and AML's EisaID registry is behind a
  paywall. GUDT is admitted to openness, so its device types registry is freely available (and compatible with already existing
  databases).

License
-------

GUDT specification (`docs` directory) and the database (`gudt.ids`), as well as the single header library (`gudt.h`) are all
Free and Open Source Software, licensed under the permissive terms of the **MIT license**.

The utilities (command line tool and GUI editor in the `src` directory and that directory alone) on the other hand are licensed
under the terms of the copyleft **GNU General Public License** version 3 or (at your option) any later version. They had to be,
because their source contain some GPL'd parts and the license mandates that the whole combined program has to be released under
the GPL.

License of the generated GUDT blobs is up to the users and could be anything, even closed source and proprietary, since the license
of the code of the program does not apply to the output.

Known Issues
------------

The `gudt` command line tool and the `gudtgui` editor both work perfectly with JSON, packed and unpacked GUDT blobs. The AML and
FDT importing feature is lacking a bit though, the parsers generate GUDT nodes correctly, but they might miss devices and/or
certain resources.

The `gudt.ids` database needs lot more device driver / universal type code records. It is more of PoC for now that the vendor id
and model id indeed can be assigned from parsing AML and FDT blobs.

Help Wanted
-----------

Looking for volunteers to help expanding and fixing the [gudt.ids](https://gitlab.com/bztsrc/gudt/raw/main/gudt.ids) database, as
well as [adding more FDT property keys](src/fdt.c) to the parser. If you want to contribute, please open a PR or just open an
[issue](https://gitlab.com/bztsrc/gudt/issues) and let me know.

That's all folks!

bzt

