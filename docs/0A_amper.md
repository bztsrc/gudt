Amper
=====

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x0a                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | 0                                                           |
|      8 |    8 | milliamper                                                  |

You can attach these nodes to any kind of device to express an Amper requirement.

JSON format
-----------

```
{
    "type": "AMPER",
    "parent": (string) parent device's name,
    "base": (number) milliamper value
}
```
