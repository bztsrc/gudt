General Purpose I/O
===================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe8                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | GPIO size                                                   |
|      8 |    8 | GPIO address                                                |

JSON format
-----------

```
{
    "type": "GPIO",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) GPIO size,
    "base": (number) GPIO address
}
```
