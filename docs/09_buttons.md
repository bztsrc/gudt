Buttons
=======

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x09                                                 |
|      1 |    1 | flags                                                       |
|      2 |    2 | parent node                                                 |
|      4 |    4 | button pressed state node                                   |
|      8 |    8 | button pressed state bitmask                                |

When not inlined, then it is usually an indirect node, specifies a button with a pressed state pointer. See [flags](flags.md).

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x09                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Each item in the inlined array is a button id. There's no particular context, you can attach these nodes to any kind of device
with a physical button.

JSON format
-----------

Button definition:

```
{
    "type": "BUTTONS",
    "parent": (string) parent device's name,
    "unit": "indirect",
    "node": (number) pointed node,
    "mask": (number) bitmask
}
```

Button reference:

```
{
    "type": "BUTTONS",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of button ids ]
}
```
