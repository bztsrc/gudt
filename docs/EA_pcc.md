PCC
===

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xea                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | PCC size                                                    |
|      8 |    8 | PCC address                                                 |

JSON format
-----------

```
{
    "type": "PCC",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) PCC size,
    "base": (number) PCC address
}
```
