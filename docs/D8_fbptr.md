Framebuffer Pointer
===================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xd8                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | packed pixel size and format                                |
|      8 |    8 | framebuffer address                                         |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
Usually a node of type 0xd9 follows. Since byte order is swapped on big endian machines, you must use bitshift and mask to get
the data from the `size` field:
- `size & 0xff`: size of one pixel in bits (implicitly bits per channel: 15=5 bits, 16=rb:5,g:6, 24,32=8 bits, 48,64=16 bits)
- `(size >> 8) & 0xff`: red channel's position in bits
- `(size >> 16) & 0xff`: green channel's position in bits
- `(size >> 24) & 0xff`: blue channel's position in bits

JSON format
-----------

```
{
    "type": "FBPTR",
    "parent": (number) parent node,
    "size": (number) pixel size and format,
    "base": (number) framebuffer address
}
```
