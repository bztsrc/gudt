Thermal
=======

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x0c                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | thermal level                                               |
|      8 |    8 | thermal temperature                                         |

For the level, 0 means `critical`, 1 not used, 2 means `warning` temperature level. The temperature is in SI Celsius. A thermal
sensor device typically has a [sensor](08_sensors.md) resource describing where to read the current value from, and a thermal
resource which describes the temperature limit.

JSON format
-----------

```
{
    "type": "THERMAL",
    "parent": (string) parent device's name,
    "level": "critical" / "warning",
    "base": (number) C° temperature value
}
```
