Reserved Memory
===============

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xde                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | area size                                                   |
|      8 |    8 | area address                                                |

This is usually not used, just a jolly joker to describe otherwise uncategoried, faulty or reserved, non-writable and therefore
unusable system memory. In combination with types 0xdd, 0xdf and 0xe0, can be used to describe a memory map.

JSON format
-----------

```
{
    "type": "RESVMEM",
    "parent": (string) parent device's name,
    "size": (number) area size,
    "base": (number) area address
}
```
