Voltage
=======

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x0b                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | 0                                                           |
|      8 |    8 | millivolts                                                  |

You can attach these nodes to any kind of device to express a Voltage requirement.

JSON format
-----------

```
{
    "type": "VOLT",
    "parent": (string) parent device's name,
    "base": (number) millivolts value
}
```
