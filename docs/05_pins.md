Pins
====

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x05                                                 |
|      1 |    1 | flags                                                       |
|      2 |    2 | parent node                                                 |
|      4 |    4 | pin state node                                              |
|      8 |    8 | pin state bitmask                                           |

When not inlined, it is usually an indirect node, specifies a pin with a state pointer. See [flags](flags.md).

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x05                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Each item in the inlined array is a pin number. There's no particular context, you can attach these nodes to any kind of device
with physical pins.

JSON format
-----------

Pin definition:

```
{
    "type": "PINS",
    "parent": (string) parent device's name,
    "unit": "indirect",
    "node": (number) pointed node,
    "mask": (number) bitmask
}
```

Pin reference:

```
{
    "type": "PINS",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of pin ids ]
}
```
