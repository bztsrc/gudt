Node Flags
==========

When a node's `type` isn't zero, then it is a resource node with a `flags` field. This `flags` field has two values:

- bit 0..3: lower tetrad `GUDT_F_UNIT`, encodes unit size in shift bits: 0 = 8 bit, 1 = 16 bit, 2 = 32 bit, 3 = 64 bit, etc.
- bit 4..7: upper tetrad `GUDT_F_DATA`, if 0 then unit is alignment, otherwise this is the number of unit sized items inlined.

Resource Descriptor
-------------------

Most common case, upper tetrad is zero and lower tetrad is not 0x0f and encodes alignment. What address space is used for the
base address depends on `type`: MMIO, IO port, IRQ, NVRAM/CMOS, etc.

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type != 0 resource node                                     |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | size field                                                  |
|      8 |    8 | address field                                               |

```
{
    "unit": "byte" / "word" / "dword" / "qword" / "m128" / "m256" / "m512" / "m1024",
    "size": (number) size,
    "base": (number) address
}
```

Indirect Reference
------------------

When the `flags` field is 0x0f `GUDT_F_INDIRECT`. Here the `size` field points to another resource node.

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type != 0 resource node                                     |
|      1 |    1 | flags = 0x0f                                                |
|      2 |    2 | parent node                                                 |
|      4 |    4 | another resource node id                                    |
|      8 |    8 | bitmask if not 0                                            |

```
{
    "unit": "indirect",
    "node": (number) pointed node
}
```

or

```
{
    "unit": "indirect",
    "node": (number) pointed node,
    "mask": (number) bitmask
}
```

Inlined Data
------------

When the upper tetrad isn't zero then it is the number of inlined items and then lower tetrad encodes item size.

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type != 0 resource node                                     |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data array                                          |

```
{
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of values ]
}
```

If a resource needs more than 12 inlined bytes or 6 words, 3 dwords or 1 qword, then simply more nodes are added to the list.
With qword items, `size` is always zero and a single qword data item is stored in the `base` field.
