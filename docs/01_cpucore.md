CPU Core
========

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x01                                                 |
|      1 |    1 | flags (bit 0..3: release address length, bit 4..7: 0)       |
|      2 |    2 | parent node                                                 |
|      4 |    4 | id (serial number or xAPIC/x2APIC id on lAPIC)              |
|      8 |    8 | release address                                             |

By writing a function's address to the memory pointed by `release address`, the given CPU core will exit its spinloop, and will
start executing that function. When this release address is zero, then other methods are used to startup the AP.

JSON format
-----------

```
{
    "type": "CPUCORE",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "sn": (number) serial or APIC id,
    "base": (number) release address
}
```
