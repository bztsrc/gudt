Device
======

Node format
-----------

In this case the node has a special format, which is as follows:

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x00                                                 |
|      1 |    1 | universal device category code                              |
|      2 |    2 | parent node                                                 |
|      4 |    2 | driver name (pointer relative to magic)                     |
|      6 |    2 | alternative driver name (pointer relative to magic)         |
|      8 |    2 | device name (pointer relative to magic)                     |
|     10 |    2 | universal device type code                                  |
|     12 |    2 | universal device vendor code                                |
|     14 |    2 | universal device model code                                 |

The name fields in this structure contain a magic relative offset, where value of 0 means not set, values 1 - 7 are invalid, and
otherwise they point to the beginning of a string in the string table. For the first element in the array (which is the root of
the device tree) category is 0xff, the motherboard's model is stored in the device name field and driver name is the manufacturer,
and the device type field store a chassis type (referred to as power management profile in ACPI).

`device category` is the same as PCI device class, except 0xff (unassigned in PCI) means "machine", root of the node tree.

`driver name` and if not zero, `alternative` are driver names that uniquely identify the device's type.

`device name` is the name of the device, only guaranteed to be unique *per driver*.

`device type` is the same as PCI device sub-class, however codes above 0xff are unique to GUDT.

`device vendor` is the same 16-bit code as PCI vendor id.

`device model` is the same 16-bit code as PCI device id.

JSON format
-----------

```
{
    "type": "DEVICE",
    "parent": (string) parent device's name,
    "category": "UNKNOWN" / "STORAGE" / "NETWORK" / "DISPLAY" / "MULTIMEDIA" / "MEMORY" / "BRIDGE" /
        "COMM" / "GENERIC" / "INPUT" / "DOCK" / "PROCESSOR" / "SERIAL" / "WIRELESS" / "INTELLIGENT" /
        "SATELLITE" / "ENCRYPTION" / "SIGNAL" / "ACCEL" / "NONESSENTIAL",
    "driver": (string) name of the driver,
    "alternative": (string) name of the driver,
    "name": (string) device name per driver,
    "device": (number) universal device type code,
    "vendor": (number) vendor id,
    "model": (number) model id
}
```

For the root node only:

```
{
    "type": "DEVICE",
    "parent": 0,
    "category": "MACHINE",
    "driver": (string) name of the manufacturer,
    "name": (string) name of the board,
    "device": "UNSPECIFIED" / "DESKTOP" / "MOBILE" / "WORKSTATION" / "ENTERPRISE" / "SOHO" / "APPLIANCE" /
        "PERFORMANCE" / "TABLET",
    "vendor": (number) vendor id,
    "model": (number) model id
}
```

If you have [pci.ids](https://pci-ids.ucw.cz/) installed, then you can also use strings in the `vendor` and `model` fields.
