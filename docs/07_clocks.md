Clocks
======

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x07                                                 |
|      1 |    1 | flags                                                       |
|      2 |    2 | parent node                                                 |
|      4 |    4 | current timer node                                          |
|      8 |    8 | 0                                                           |

When not inlined, then it is usually an indirect node, specifies a clock with a current timer pointer. See [flags](flags.md).

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x07                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Each item in the inlined array is a clock id. There's no particular context, you can attach these nodes to any kind of device.

JSON format
-----------

Clock definition:

```
{
    "type": "CLOCKS",
    "parent": (string) parent device's name,
    "unit": "indirect",
    "node": (number) pointed node
}
```

Clock reference:

```
{
    "type": "CLOCKS",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of clock ids ]
}
```
