LEDs
====

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x06                                                 |
|      1 |    1 | flags                                                       |
|      2 |    2 | parent node                                                 |
|      4 |    4 | LED state node                                              |
|      8 |    8 | LED state bitmask                                           |

When not inlined, then it is usually an indirect node, specifies a LED with a state pointer. See [flags](flags.md).

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x06                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Each item in the inlined array is a LED id. There's no particular context, you can attach these nodes to any kind of device
with LEDs.

JSON format
-----------

LED definition:

```
{
    "type": "LEDS",
    "parent": (string) parent device's name,
    "unit": "indirect",
    "node": (number) pointed node,
    "mask": (number) bitmask
}
```

LED reference:

```
{
    "type": "LEDS",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of LED ids ]
}
```
