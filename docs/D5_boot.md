Boot Partition
==============

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xd5                                                 |
|      1 |    1 | flags = 0x13                                                |
|      2 |    2 | parent node = 0                                             |
|      4 |    4 | must be zero                                                |
|      8 |    8 | first part of boot partition's UUID                         |
|     16 |    1 | type = 0xd5                                                 |
|     17 |    1 | flags = 0x13                                                |
|     18 |    2 | parent node = 0                                             |
|     20 |    4 | must be zero                                                |
|     24 |    8 | second part of boot partition's UUID                        |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
It always has two nodes and contains inlined qword data, the UniquePartitionGUID field in the GPT of the boot partition. If
the booting does not use a GUID Partitioning Table, then it must be generated as
`54524150-(device code)-(partition number)-616F6F7400000000`.

JSON format
-----------

```
{
    "type": "BOOT",
    "parent": 0,
    "unit": "qword",
    "data": [ (number) first part, (number) second part ]
}
```
