Generic Serial Bus
==================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe9                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | GSB size                                                    |
|      8 |    8 | GSB address                                                 |

JSON format
-----------

```
{
    "type": "GSB",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) GSB size,
    "base": (number) GSB address
}
```
