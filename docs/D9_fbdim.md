Framebuffer Dimension
=====================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xd9                                                 |
|      1 |    1 | flags = 0x32                                                |
|      2 |    2 | parent node                                                 |
|      4 |    4 | width (pixels)                                              |
|      8 |    4 | height (pixels)                                             |
|     12 |    4 | pitch (bytes in a scanline)                                 |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
Usually added after a 0xd8 node. It always has inlined dword data.

JSON format
-----------

```
{
    "type": "FBDIM",
    "parent": (number) parent node,
    "unit": "dword",
    "data": [ (number) width, (number) height, (number) pitch ]
}
```
