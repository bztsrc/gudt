Non-volatile System Memory
==========================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xdd                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | area size                                                   |
|      8 |    8 | area address                                                |

This memory area must be preserved during hybernation. In combination with types 0xde, 0xdf and 0xe0, can be used to describe a
memory map.

Do not confuse with [NVRAM](E5_nvram.md) (type 0xe5), this memory region must be preserved by the operating system.

JSON format
-----------

```
{
    "type": "NVSMEM",
    "parent": (string) parent device's name,
    "size": (number) area size,
    "base": (number) area address
}
```
