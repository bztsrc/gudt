PCI Configuration
=================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe2                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |   12 | PCI configuration                                           |

JSON format
-----------

```
{
    "type": "PCI",
    "parent": (string) parent device's name,
    "size": (number) PCI configuration size,
    "base": (number) PCI configuration address
}
```
