PCI BAR Address
===============

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe6                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | PCIBAR size                                                 |
|      8 |    8 | PCIBAR address                                              |

JSON format
-----------

```
{
    "type": "PCIBAR",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) PCIBAR size,
    "base": (number) PCIBAR address
}
```
