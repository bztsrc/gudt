Boot Module
===========

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xda                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node = 0                                             |
|      4 |    4 | boot module size                                            |
|      8 |    8 | boot module address                                         |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
It contains the absolute memory address and size of an additional file that has been loaded alongside the kernel (usally an
initial ramdisk image). Multiple nodes might exists with this type, one for each file.

JSON format
-----------

```
{
    "type": "MODULE",
    "parent": 0,
    "size": (number) boot module size,
    "base": (number) boot module address
}
```
