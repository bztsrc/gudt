IPMI
====

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe7                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | IPMI size                                                   |
|      8 |    8 | IPMI address                                                |

JSON format
-----------

```
{
    "type": "IPMI",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) IPMI size,
    "base": (number) IPMI address
}
```
