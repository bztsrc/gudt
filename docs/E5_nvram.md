Non-Volatile RAM (CMOS)
=======================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe5                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | NVRAM/CMOS size                                             |
|      8 |    8 | NVRAM/CMOS address                                          |

Do not confuse with [NVSMEM](DD_nvsmem.md) (type 0xdd), this NVRAM is preserved by the hardware.

JSON format
-----------

```
{
    "type": "NVRAM",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) NVRAM/CMOS size,
    "base": (number) NVRAM/CMOS address
}
```
