Level 3 Cache
=============

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x11                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: 3)                    |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Stores the L3 cache size, line size and number of sets.

JSON format
-----------

```
{
    "type": "L3CACHE",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) cache size, (number) line size, (number) number of sets ]
}
```
