I/O Port
========

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe1                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | IO port size                                                |
|      8 |    8 | IO port address                                             |

JSON format
-----------

```
{
    "type": "IOPORT",
    "parent": (string) parent device's name,
    "size": (number) IO port size,
    "base": (number) IO port address
}
```
