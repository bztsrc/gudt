Frequency
=========

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x0d                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | divisor                                                     |
|      8 |    8 | clock frequency                                             |

Lot of devices specify frequency in terms of a clock frequency and a divisor. You can attach this node to such devices.
If `div` is zero, then no divisor is used and `base` is the absolute frequency in milliHz (so `base` of 1000 = 1 Hz).

JSON format
-----------

```
{
    "type": "FREQ",
    "parent": (string) parent device's name,
    "div": (number) divisor or zero,
    "base": (number) clock frequency milliHz
}
```
