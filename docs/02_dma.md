DMA
===

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x02                                                 |
|      1 |    1 | flags (bit 0..3: transfer units)                            |
|      2 |    2 | parent node                                                 |
|      4 |    4 | DMA transfer width                                          |
|      8 |    8 | DMA transfer address                                        |

JSON format
-----------

```
{
    "type": "DMA",
    "parent": (string) parent device's name,
    "size": (number) transfer width,
    "base": (number) transfer address
}
```
