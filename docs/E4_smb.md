System Management Bus
=====================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe4                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | SMB size                                                    |
|      8 |    8 | SMB address                                                 |

JSON format
-----------

```
{
    "type": "SMB",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) SMB size,
    "base": (number) SMB address
}
```
