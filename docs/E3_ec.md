Embedded Controller
===================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe3                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | EC size                                                     |
|      8 |    8 | EC address                                                  |

JSON format
-----------

```
{
    "type": "EC",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) EC size,
    "base": (number) EC address
}
```
