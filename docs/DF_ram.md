Free Usable Memory
==================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xdf                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | area size                                                   |
|      8 |    8 | area address                                                |

This memory area is free to be used by the operating system. In combination with types 0xdd, 0xde and 0xe0, can be used to describe
a memory map.

JSON format
-----------

```
{
    "type": "RAM",
    "parent": (string) parent device's name,
    "size": (number) area size,
    "base": (number) area address
}
```
