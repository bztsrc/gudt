Interrupt Controller
====================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0x04                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | interrupt controller node                                   |
|      8 |    8 | IRQ or 0xffffffffffffffff                                   |

This does not describe an interrupt controller, instead in systems with multiple interrupt controllers, this node can be added to
a device (indicated by `parent node`) to express which controller (indicated by `interrupt controller node`) it is connected to.
If IRQ is given, then only that IRQ is connected to the specified controller, this is used when a device is connected to multiple
interrupt controllers. If all bits of IRQ is set, then that means all IRQs of the device are connected to the same controller. The
IRQs that a device can generate are specified in the IRQ resource nodes (type 0x03).

JSON format
-----------

```
{
    "type": "INTC",
    "parent": (string) parent device's name,
    "node": (string) interrupt controller device's name,
    "base": (number) IRQ or 0xffffffffffffffff
}
```

NOTE: looks similar to [indirect reference](flags.md) node descriptors, but here the `size` must point to a device node and not to
a resource node, and `base` isn't a bitmask rather an interrupt specifier.
