Memory Mapped I/O
=================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xe0                                                 |
|      1 |    1 | flags (bit 0..3: alignment, bit 4..7: 0)                    |
|      2 |    2 | parent node                                                 |
|      4 |    4 | MMIO size                                                   |
|      8 |    8 | MMIO address                                                |

Memory mapped device registers. In combination with types 0xdd, 0xde and 0xdf, can be used to describe a memory map.

JSON format
-----------

```
{
    "type": "MMIO",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) MMIO size,
    "base": (number) MMIO address
}
```
