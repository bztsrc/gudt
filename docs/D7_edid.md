Framebuffer EDID
===================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xd7                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node                                                 |
|      4 |    4 | EDID buffer size                                            |
|      8 |    8 | EDID buffer address                                         |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
The EDID buffer contains timing information for the attached monitor and thus specifies the maximum resolution it can display.
On configurations with multiple monitors, multiple nodes with this type should exists.

JSON format
-----------

```
{
    "type": "EDID",
    "parent": (number) parent node,
    "size": (number) EDID buffer size,
    "base": (number) EDID buffer address
}
```
