Default
=======

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xdc                                                 |
|      1 |    1 | flags (bit 0..3: default alignment, bit 4..7: 0)            |
|      2 |    2 | parent node                                                 |
|      4 |    4 | default size                                                |
|      8 |    8 | default address                                             |

There's no particular context, you can attach these nodes to any kind of device.

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xdc                                                 |
|      1 |    1 | flags (bit 0..3: item size, bit 4..7: number of items)      |
|      2 |    2 | parent node                                                 |
|      4 |   12 | inlined data                                                |

Might also contain inlined data.

JSON format
-----------

```
{
    "type": "DEFAULT",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "size": (number) default size,
    "base": (number) default address
}
```

or

```
{
    "type": "DEFAULT",
    "parent": (string) parent device's name,
    "unit": "byte" / "word" / "dword" / "qword",
    "data": [ (number) list of default values ]
}
```
