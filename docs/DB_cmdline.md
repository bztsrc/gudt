Boot Command line
=================

Node format
-----------

| Offset | Size | Description                                                 |
|-------:|-----:|-------------------------------------------------------------|
|      0 |    1 | type = 0xdb                                                 |
|      1 |    1 | flags = 0                                                   |
|      2 |    2 | parent node = 0                                             |
|      4 |    4 | boot command line size                                      |
|      8 |    8 | boot command line address                                   |

This node should not appear in GUDT blobs on disk, rather it is added by the boot loader before it passes the GUDT to a kernel.
If `base` is smaller than header size (65536 tops) then it points to a string in the string table, otherwise it is an absolute
memory address.

JSON format
-----------

```
{
    "type": "CMDLINE",
    "parent": 0,
    "size": (number) boot command line size,
    "base": (number) boot command line address
}
```
