/*
 * src/file.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief File operations
 */

#include "main.h"
#ifdef __WIN32__
#include <windows.h>
#endif

extern int verbose;

/**
 * Read a file entirely into memory
 */
uint8_t* readfileall(char *file)
{
#ifdef __WIN32__
    HANDLE f;
    DWORD r, t;
    int i;
#else
    FILE *f;
#endif
    uint8_t *data = NULL;
    uint32_t read_size = 0;

    if(!file || !*file) return NULL;
#ifdef __WIN32__
    memset(&szFile, 0, sizeof(szFile));
    MultiByteToWideChar(CP_UTF8, 0, file, -1, szFile, PATH_MAX);
    for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
    f = CreateFileW(szFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if(f != INVALID_HANDLE_VALUE) {
        r = GetFileSize(f, NULL);
        read_size = (int64_t)r;
        data = (unsigned char*)malloc(read_size + 64);
        if(!data) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
        if(!ReadFile(f, data, r, &t, NULL)) t = 0;
        read_size = (int64_t)t;
        memset(data + read_size, 0, 64);
        CloseHandle(f);
    }
#else
    f = fopen(file, "rb");
    if(f) {
        fseek(f, 0L, SEEK_END);
        read_size = (int64_t)ftell(f);
        fseek(f, 0L, SEEK_SET);
        data = (unsigned char*)malloc(read_size + 64);
        if(!data) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
        read_size = (int64_t)fread(data, 1, read_size, f);
        memset(data + read_size, 0, 64);
        fclose(f);
    }
#endif
    if(!read_size && data) { free(data); data = NULL; }
    if(verbose && !data) msg("unable to read '%s'\r\n", file);
    return data;
}

/**
 * Write a memory buffer entirely into file
 */
int writefileall(char *file, uint8_t *data, uint32_t size)
{
#ifdef __WIN32__
    HANDLE f;
    DWORD r, t = 0;
#else
    FILE *f;
#endif
    uint32_t i;

    if(!file || !*file || !data || size < 1) return 0;
#ifdef __WIN32__
    memset(&szFile, 0, sizeof(szFile));
    MultiByteToWideChar(CP_UTF8, 0, file, -1, szFile, PATH_MAX);
    for(i = 0; szFile[i]; i++) if(szFile[i] == L'/') szFile[i] = L'\\';
    r = size;
    f = CreateFileW(szFile, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if(f != INVALID_HANDLE_VALUE) {
        if(!WriteFile((HANDLE)f, data, r, &t, NULL) || r != t) t = 0;
        CloseHandle(f);
        return r == t;
    }
#else
    f = fopen(file, "wb");
    if(f) {
        i = (uint32_t)fwrite(data, 1, size, f);
        fclose(f);
        return i == size;
    }
#endif
    msg("unable to write '%s'\r\n", file);
    return 0;
}
