Grand Unified Device Tree Utilities
===================================

Compilation
-----------

```
make
```

Will compile the `gudt` command line utility. Has no dependencies and written for both POSIX and WIN32 API (using mingw).

```
USE_GUI=1 make
```

Will compile the `gudtgui` windowed editor. Depends on GTK (and with that, on pkg-config, gdk, gio, pango, cairo, harfbuzz,
fontconfig, pixman, pcre etc. etc. etc.).

```
DEBUG=1 make
DEBUG=1 USE_GUI=1 make
```

For both, specifying `DEBUG` environment variable will compile with debug symbols and debug run-time messages (you can enable
this latter with `-vvv`, mostly useful for the AML/FDT importers).

License
-------

Unlike the rest, this directory and this directory alone is licensed under the terms of the copyleft **GNU General Public License**
version 3 or (at your option) any later version.
