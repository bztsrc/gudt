/*
 * src/fdt.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Flattened Device Tree parser
 *
 * Features DELIBERATELY not implemented (because they are stupid):
 * - status: this is a dynamic run-time property, makes absolutely no sense to store it on disk
 * - bootargs: should be handled by the boot loader in memory and not stored on disk, has nothing to do
 *      with the device tree, it is Linux specific and ain't FDT blobs OS-independent in the first place?
 */

#include "main.h"

typedef struct {
    char *name, *drv, *alt;
    uint32_t parent, intparent, addrcells, sizecells, intcells, dmacells, clkcells, gpiocells;
    uint32_t type, phandle, phparent;
    uint32_t mapsz[2], *map[2], cpucache[6];
    uint64_t cpurel;
} fdt_t;
static fdt_t *fdt_nodes;
static uint32_t fdt_numnodes;
/* property getters */
static uint64_t fdt_value(uint32_t *ptr, uint32_t size);
static uint64_t fdt_addr(uint32_t *ptr, uint32_t size, int map, fdt_t *node);
static uint32_t fdt_phandle(uint32_t *ptr);

/**
 * The real deal, convert an FDT node properties into a GUDT node
 */
static void fdt_prop(char *name, uint8_t *value, uint32_t size, fdt_t *node)
{
    uint64_t a, s;
    uint32_t as, ss, is, i, j, n = size >> 2, *p = (uint32_t*)value, parent;

    if(value && size && node->drv && *node->drv) {
        parent = gudt_parent(fdt_nodes[node->parent].name, fdt_nodes[node->parent].drv);
        as = fdt_nodes[node->parent].addrcells; ss = fdt_nodes[node->parent].sizecells; is = fdt_nodes[node->intparent].intcells;
        if(!memcmp(name, "reg", 4) && as) {
            for(i = 0; i < n;) {
                a = fdt_addr(&p[i], as, 0, node); i += as;
                s = fdt_value(&p[i], ss); i += ss;
                switch(node->type) {
                    case 1:
                        gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_CPUCORE, node->cpurel, a);
                        for(j = 0; j < 2; j++)
                            if(node->cpucache[j * 3])
                                gudt_resource(node->name, node->drv, node->alt, parent, 0x32, GUDT_T_L0CACHE + j,
                                    (uintptr_t)&node->cpucache[j * 3], 3);
                    break;
                    case 2: mem_add(GUDT_T_RAM, a, s); break;
                    case 3: gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_NVRAM, a, s); break;
                    default: gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_MMIO, a, s); break;
                }
            }
        } else
        if(!memcmp(name, "dmas", 5) && as) {
            for(i = 0; i < n;) {
                a = fdt_addr(&p[i], as, 1, node); i += as;
                s = fdt_value(&p[i], ss); i += ss;
                gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_DMA, a, s);
            }
        } else
        if(!memcmp(name, "interrupts-extended", 20)) {
            for(i = 0; i < n;) {
                j = fdt_phandle(&p[i]); i++;
                if(!fdt_nodes[j].intcells) break;
                if(fdt_nodes[j].intcells > 2) i += fdt_nodes[j].intcells - 2;
                a = fdt_value(&p[i], 1); i++;
                if(fdt_nodes[j].intcells > 1) { s = fdt_value(&p[i], 1); i++; } else s = 0;
                gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_IRQ, a, 0/*s*/);
                gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_INTC, a, j);
            }
        } else
        if(!memcmp(name, "interrupts", 11) && is) {
            for(i = 0; i < n;) {
                if(is > 2) i += is - 2;
                a = fdt_value(&p[i], 1); i++;
                if(is > 1) { s = fdt_value(&p[i], 1); i++; } else s = 0;
                gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_IRQ, a, 0/*s*/);
            }
            gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_INTC, -1UL, node->intparent);
        } else
        if(!memcmp(name, "clocks", 7) && node->clkcells) {
        } else
        if(!memcmp(name, "clock-frequency", 16)) {
            gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_FREQ, fdt_value((uint32_t*)value, 1), 0);
        } else
        if(!memcmp(name, "thermal-sensors", 16)) {
            gudt_resource(node->name, node->drv, node->alt, parent, 0x12, GUDT_T_SENSORS, (uintptr_t)value, 1);
        } else
        if(!memcmp(name, "temperature", 12)) {
            gudt_resource(node->name, node->drv, node->alt, parent, 0, GUDT_T_THERMAL, fdt_value((uint32_t*)value, 1), (node->type >> 8));
        } else
        if(verbose) msg("unhandled property \"%s\" \"%s\" \"%s\"\r\n", node->drv, node->name, name);
    }
}

/**
 * Get value (from a property data)
 */
static uint64_t fdt_value(uint32_t *ptr, uint32_t size)
{
    uint64_t r = 0;
    uint8_t *p = (uint8_t*)ptr;

    if(size) {
        r = ((uint64_t)p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3];
        if(size > 1) { p += 4; r <<= 32; r |= ((uint64_t)p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3]; }
    }
    return r;
}

/**
 * Get address (get value from property data and translate to parent address space)
 */
static uint64_t fdt_addr(uint32_t *ptr, uint32_t size, int map, fdt_t *node)
{
    uint32_t i, siz, as;
    uint64_t a = fdt_value(ptr, size), c, b, s;

    if(node->parent) {
        node = &fdt_nodes[node->parent];
        as = fdt_nodes[node->parent].addrcells;
        while(1) {
            if(node->map[map] && node->mapsz[map] && node->sizecells) {
                siz = node->mapsz[map] >> 2;
                for(i = 0; i < siz;) {
                    c = fdt_value(&node->map[map][i], node->addrcells); i += node->addrcells;
                    b = fdt_value(&node->map[map][i], as); i += as;
                    s = fdt_value(&node->map[map][i], node->sizecells); i += node->sizecells;
                    if(a >= c && a < c + s) { a = a - c + b; break; }
                }
            }
            if(!node->parent || node->parent >= fdt_numnodes || node == &fdt_nodes[node->parent]) break;
            node = &fdt_nodes[node->parent];
        }
    }
    return a;
}

/**
 * Get a phandle (return node index for phandle in property data)
 */
static uint32_t fdt_phandle(uint32_t *ptr)
{
    uint32_t i;
    if(ptr && *ptr)
        for(i = 0; i < fdt_numnodes; i++)
            if(*ptr == fdt_nodes[i].phandle)
                return i;
    return 0;
}

/**
 * Parse FDT nodes
 */
void fdt_parse(uint8_t *ptr, char *fn)
{
    fdt_t *node;
    uint32_t parent[AML_MAXLEVEL];
    uint32_t totalsize = ptr[7] | (ptr[6] << 8) | (ptr[5] << 16);
    uint32_t off_dt = ptr[11] | (ptr[10] << 8) | (ptr[9] << 16);
    uint32_t off_str = ptr[15] | (ptr[14] << 8) | (ptr[13] << 16);
    uint32_t off_rsvmap = ptr[19] | (ptr[18] << 8) | (ptr[17] << 16);
    uint32_t version = ptr[23] | (ptr[22] << 8) | (ptr[21] << 16);
    uint32_t siz_str = ptr[35] | (ptr[34] << 8) | (ptr[33] << 16);
    uint32_t siz_dt = ptr[39] | (ptr[38] << 8) | (ptr[37] << 16);
    uint32_t siz_rsvmap = (off_dt < off_str ? off_dt : off_str) - off_rsvmap;
    uint8_t *p = ptr + off_dt, *t, *s, *end = ptr + off_dt + siz_dt;
    uint32_t i, chassis = 0, tag, sz, idx, d = 0;
    uint64_t addr, size;
#ifdef DEBUG
    uint32_t j, k, l;
#endif

    if(!(ptr[0] == 0xD0 && ptr[1] == 0x0D && ptr[2] == 0xFE && ptr[3] == 0xED)) {
        msg("%s: not a valid FDT\r\n", fn);
        return;
    }

    if(version < 17) {
        msg("%s: FDT version %u not supported\r\n", fn, version);
        return;
    }
    for(i = 0; i < siz_rsvmap; i += 16) {
        p = ptr + off_rsvmap + i;
        addr = p[7] | (p[6] << 8) | (p[5] << 16) | (p[4] << 24) | ((uint64_t)p[3] << 32) | ((uint64_t)p[2] << 40);
        p += 8;
        size = p[7] | (p[6] << 8) | (p[5] << 16) | (p[4] << 24) | ((uint64_t)p[3] << 32) | ((uint64_t)p[2] << 40);
        if(addr == 0 && size == 0) break;
        mem_add(GUDT_T_RESVMEM, addr, size);
    }

    if(end > ptr + totalsize) end = ptr + totalsize;

    /* first, count nodes */
    for(fdt_numnodes = 0, p = ptr + off_dt; p < end; ) {
        tag = p[3] | (p[2] << 8) | (p[1] << 16) | (p[0] << 24); p += 4;
        if(tag == 9) break;
        switch(tag) {
            case 1: fdt_numnodes++; for(; p < end && *p; p++){} p = (uint8_t*)(((uintptr_t)p + 3) & ~3); break;
            case 3: sz = p[3] | (p[2] << 8) | (p[1] << 16); p += 8; p = (uint8_t*)(((uintptr_t)p + sz + 3) & ~3); break;
        }
    }
    if(!fdt_numnodes) {
        if(verbose) msg("no FDT nodes found\r\n");
        return;
    }
    fdt_nodes = (fdt_t*)__builtin_alloca((fdt_numnodes + 1) * sizeof(fdt_t));
    memset(fdt_nodes, 0, (fdt_numnodes + 1) * sizeof(fdt_t));
    memset(parent, 0, sizeof(parent));

    /* second run, collect phandle references in advance */
    for(idx = 0, p = ptr + off_dt, node = fdt_nodes; p < end && idx < fdt_numnodes; ) {
        tag = p[3] | (p[2] << 8) | (p[1] << 16) | (p[0] << 24); p += 4;
        if(tag == 9) break;
        switch(tag) {
            case 1: for(node++, idx++; p < end && *p; p++){} p = (uint8_t*)(((uintptr_t)p + 3) & ~3); break;
            case 3:
                sz = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                i = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                t = p; p = (uint8_t*)(((uintptr_t)p + sz + 3) & ~3);
                s = ptr + off_str + i;
                if(i < siz_str - 1) {
                    if(!memcmp(s, "#interrupt-cells", 17)) { node->intcells = t[sz - 1]; } else
                    if(!memcmp(s, "interrupt-parent", 17)) { memcpy(&node->phparent, t, 4); } else
                    if(!memcmp(s, "phandle", 8) || !memcmp(s, "linux,phandle", 14)) { memcpy(&node->phandle, t, 4); }
                }
            break;
        }
    }

    /* third run, actually flatten the so called "flattened" device tree, parse all properties */
    for(idx = d = 0, p = ptr + off_dt, node = fdt_nodes; p < end && idx < fdt_numnodes; ) {
        tag = p[3] | (p[2] << 8) | (p[1] << 16) | (p[0] << 24); p += 4;
        if(tag == 9) break;
        switch(tag) {
            case 1:
                node++;
                if(d) parent[d] = parent[d - 1];
                node->addrcells = 2; node->sizecells = 1;
                node->parent = parent[d];
                if(node->phparent) node->intparent = fdt_phandle(&node->phparent);
                else node->intparent = fdt_nodes[node->parent].intparent;
                if(!node->intparent) node->intparent = node->parent;
                for(node->name = (char*)p; p < end && *p; p++){} p = (uint8_t*)(((uintptr_t)p + 3) & ~3);
                if(!memcmp(node->name, "cpu@", 4)) node->type = 1;
                if(!memcmp(node->name, "memory@", 7)) node->type = 2;
                if(!memcmp(node->name, "nvram@", 6)) node->type = 3;
                parent[d++] = ++idx;
            break;
            case 2: d--; break;
            case 3:
                sz = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                i = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                t = p; p = (uint8_t*)(((uintptr_t)p + sz + 3) & ~3);
                s = ptr + off_str + i;
                if(i < siz_str - 1) {
                    if(!memcmp(s, "#address-cells", 15)) { node->addrcells = t[sz - 1]; } else
                    if(!memcmp(s, "#size-cells", 12)) { node->sizecells = t[sz - 1]; } else
                    if(!memcmp(s, "#dma-cells", 11)) { node->dmacells = t[sz - 1]; } else
                    if(!memcmp(s, "#clock-cells", 13)) { node->clkcells = t[sz - 1]; } else
                    if(!memcmp(s, "#gpio-cells", 12)) { node->gpiocells = t[sz - 1]; } else
                    if((!memcmp(s, "model", 6) || !memcmp(s, "name", 5)) && !*node->name) { node->name = (char*)t; } else
                    if(!memcmp(s, "device_type", 12)) {
                        if(!memcmp(t, "cpu", 4)) node->type = 1;
                        if(!memcmp(t, "memory", 7)) node->type = 2;
                        if(!memcmp(t, "nvram", 6)) node->type = 3;
                    } else
                    if(!memcmp(s, "type", 5)) {
                        if(!memcmp(t, "critical", 9)) node->type |= 0x000;
                        if(!memcmp(t, "warning", 8)) node->type |= 0x200;
                    } else
                    if(!memcmp(s, "cpu-release", 11)) { node->type = 1; node->cpurel = fdt_value((uint32_t*)t, 2); } else
                    if(!memcmp(s, "d-cache-size", 13)) { node->cpucache[0] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "d-cache-line-size", 18)) { node->cpucache[1] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "d-cache-sets", 13)) { node->cpucache[2] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "i-cache-size", 13)) { node->cpucache[3] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "i-cache-line-size", 18)) { node->cpucache[4] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "i-cache-sets", 13)) { node->cpucache[5] = fdt_value((uint32_t*)t, 1); } else
                    if(!memcmp(s, "chassis-type", 13)) {
                        if(!memcmp(t, "desktop", 8)) chassis = GUDT_D_DESKTOP; else
                        if(!memcmp(t, "laptop", 7)) chassis = GUDT_D_MOBILE; else
                        if(!memcmp(t, "server", 7)) chassis = GUDT_D_ENTERPRISE; else
                        if(!memcmp(t, "tablet", 7)) chassis = GUDT_D_TABLET; else
                        if(!memcmp(t, "embedded", 9)) chassis = GUDT_D_APPLIANCE;
                    } else
                    if(!memcmp(s, "ranges", 7)) {
                        if(sz) {
                            node->map[0] = (uint32_t*)t;
                            node->mapsz[0] = sz;
                        }
                    } else
                    if(!memcmp(s, "dma-ranges", 11)) {
                        if(sz) {
                            node->map[1] = (uint32_t*)t;
                            node->mapsz[1] = sz;
                        }
                    } else
                    if(!memcmp(s, "compatible", 11)) {
                        for(node->drv = (char*)t; *t && t < p; t++);
                        node->alt = !*t && t[1] && t + 2 < p ? (char*)t + 1 : NULL;
                    }
                }
            break;
        }
    }

    /* fourth times the charm, do the actual parsing */
    for(idx = d = 0, p = ptr + off_dt, node = fdt_nodes; p < end && idx < fdt_numnodes; ) {
        tag = p[3] | (p[2] << 8) | (p[1] << 16) | (p[0] << 24); p += 4;
        if(tag == 9) break;
        switch(tag) {
            case 1:
                node++; idx++;
                for(; p < end && *p; p++){} p = (uint8_t*)(((uintptr_t)p + 3) & ~3);
#ifdef DEBUG
                if(verbose) {
                    printf("\r\n");
                    for(l = 0; l < d; l++) printf("  ");
                    printf("node %u name '%s' drv '%s' alt '%s'\r\n", idx, node->name, node->drv, node->alt);
                    for(l = 0; l < d; l++) printf("  ");
                    printf(" parent %u (acells %u) intparent %u (icells %u) phandle %x phparent %x\r\n",
                        node->parent, fdt_nodes[node->parent].addrcells, node->intparent, fdt_nodes[node->intparent].intcells,
                        node->phandle, node->phparent);
                    for(l = 0; l < d; l++) printf("  ");
                    printf(" acells %u scells %u icells %u dcells %u ccells %u gcells %u\r\n",
                        node->addrcells, node->sizecells, node->intcells, node->dmacells, node->clkcells, node->gpiocells);
                    for(j = 0; j < 2; j++)
                        if(node->map[j] && node->sizecells) {
                            for(l = 0; l < d; l++) printf("  ");
                            printf(" map[%u] offs %08x size %3d", j, (uint32_t)((uint8_t*)node->map[j] - ptr), node->mapsz[j]);
                            l = node->mapsz[j] >> 2;
                            for(k = 0; k < l;) {
                                printf(" <%lx", fdt_value(&node->map[j][k], node->addrcells)); k += node->addrcells;
                                printf(",%lx", fdt_value(&node->map[j][k], fdt_nodes[node->parent].addrcells)); k += fdt_nodes[node->parent].addrcells;
                                printf(",%lx>", fdt_value(&node->map[j][k], node->sizecells)); k += node->sizecells;
                            }
                            printf("\r\n");
                        }
                }
#endif
                d++;
                /* add root node */
                if(!gudt_hdr.hdr.numnodes)
                    gudt_resource(node->name, node->drv, node->alt, 0, 0, -1, chassis, 0);
            break;
            case 2: d--; break;
            case 3:
                sz = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                i = p[3] | (p[2] << 8) | (p[1] << 16); p += 4;
                t = p; p = (uint8_t*)(((uintptr_t)p + sz + 3) & ~3);
                s = ptr + off_str + i;
                if(i < siz_str - 1) {
#if DEBUG
                    if(verbose) {
                        for(i = 0; i < d; i++) printf("  ");
                        printf(" prop offs %08x size %3d %s", (uint32_t)(t - ptr), sz, (char*)s);
                        if(sz) { printf(" <"); for(i = 0; i < sz; i++) { printf("%s%02x", i ? "," : "", t[i]); } printf(">"); }
                        printf("\r\n");
                    }
#endif
                    /* filter out all properties that we have already parsed */
                    if(memcmp(s, "#address-cells", 15) && memcmp(s, "#size-cells", 12) && memcmp(s, "#dma-cells", 11) &&
                      memcmp(s, "#clock-cells", 13) && memcmp(s, "#gpio-cells", 12) &&
                      memcmp(s, "model", 6) && memcmp(s, "name", 5) && memcmp(s, "device_type", 12) &&
                      memcmp(s, "cpu-release", 11) && memcmp(s + 1, "-cache-", 7) && memcmp(s, "chassis-type", 13) &&
                      memcmp(s, "ranges", 7) && memcmp(s, "dma-ranges", 11) && memcmp(s, "compatible", 11) &&
                      memcmp(s, "#interrupt-cells", 17) && memcmp(s, "interrupt-parent", 17) &&
                      memcmp(s, "phandle", 8) && memcmp(s, "linux,phandle", 14) &&
                      memcmp(s, "status", 7) && node->name && *node->name &&
                      memcmp(node->name, "aliases", 8) && memcmp(node->name, "chosen", 7) &&
                      memcmp(node->name, "__symbols__", 12) && memcmp(node->name, "__overrides__", 14))
                        fdt_prop((char*)s, t, sz, node);
                }
            break;
        }
    }

    /* fix up interrupt controller references, which still have fdt_node index (might be forward references) */
    for(i = 0; i < gudt_hdr.hdr.numnodes; i++)
        if(gudt_nodes[i].type == GUDT_T_INTC)
            gudt_nodes[i].r.p.size = gudt_parent(fdt_nodes[gudt_nodes[i].r.p.size].name, fdt_nodes[gudt_nodes[i].r.p.size].drv);
}
