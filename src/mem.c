/*
 * src/mem.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Memory map functions
 */

#include "main.h"

typedef struct {
    uint64_t base;
    uint64_t size;
    uint32_t type;
} memmap_t;

memmap_t *mem = NULL;
int nummem = 0;

/**
 * Comparator
 */
int mmcmp(const void *a, const void *b)
{
    /* substract won't work here, because base is 64-bits, but returned int is just 32 bits */
    return ((memmap_t*)a)->base == ((memmap_t*)b)->base ? 0 : (((memmap_t*)a)->base > ((memmap_t*)b)->base ? 1 : -1);
}

/**
 * Add a memory region to list
 */
void mem_add(uint32_t type, uint64_t base, uint64_t size)
{
    int i;

    if(!size) return;

    if(verbose > 1)
        msg("memory type %02x base %08x%08x size %08x%08x\r\n", type, (uint32_t)(base >> 32), (uint32_t)base,
            (uint32_t)(size >> 32), (uint32_t)size);

    /* let's see if we can merge it with an already existing memory area */
    for(i = 0; i < nummem; i++)
        if(mem[i].type == type) {
            if(mem[i].base <= base && mem[i].base + mem[i].size >= base + size) return;
            if(mem[i].base >= base && mem[i].base + mem[i].size <= base + size) { mem[i].base = base; mem[i].size = size; return; }
            if(mem[i].base == base + size) { mem[i].base = base; mem[i].size += size; return; }
            if(base == mem[i].base + mem[i].size) { mem[i].size += size; return; }
        }

    /* add as a new area */
    i = nummem++;
    mem = (memmap_t*)realloc(mem, nummem * sizeof(memmap_t));
    if(!mem) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
    mem[i].base = base;
    mem[i].size = size;
    mem[i].type = type;
}

/**
 * Add memory regions to nodes
 */
void mem_tonodes(void)
{
    gudt_device_t *devs = (gudt_device_t*)gudt_nodes;
    int i;

    if(mem) {
        qsort(mem, nummem, sizeof(memmap_t), mmcmp);
        for(i = 0; i < nummem; i++)
            gudt_add(devs ? devs->name : 8, devs ? devs->driver : 0, 1, 0, 0, mem[i].type, mem[i].base, mem[i].size);
    }
}

/**
 * Free resources
 */
void mem_free(void)
{
    if(mem) {
        free(mem); mem = NULL;
    }
    nummem = 0;
}
