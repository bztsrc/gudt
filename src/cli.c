/*
 * src/cli.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Command line Grand Unified Device Tree utility
 */

#include "main.h"

static const char *copyright =
    "Grand Unified Device Tree tool, Copyright (c) 2024 bzt, GPLv3+\r\nhttps://gitlab.com/bztsrc/gdut\r\n\r\n";

int verbose = 0;

/**
 * Add error message to log
 */
void msg(const char *fmt, ...)
{
    __builtin_va_list args;
    __builtin_va_start(args, fmt);
    fprintf(stderr, "gudt: ");
    vfprintf(stderr, fmt, args);
    __builtin_va_end(args);
}

/**
 * Free message log
 */
void msg_free(void)
{
    /* nothing to do */
}

/**
 * Usage instructions
 */
void usage(void)
{
    printf("%s", copyright);
    printf("  gdut [-v[v[v]]] [-d] [<-j|-J|-o|-u> <outfile>] <input> [<input> [...]]\r\n\r\n");
    printf("                print device tree to stdout (default)\r\n");
    printf("  -v            verbose output (-vv, -vvv more details)\r\n");
    printf("  -d            dump the universal device type database to stdout\r\n");
    printf("  -j <outfile>  output JSON text (-J without comments)\r\n");
    printf("  -o <outfile>  output packed GUDT blob\r\n");
    printf("  -u <outfile>  output unpacked GUDT blob\r\n");
    printf("  <input>       import and merge DSDT/SSDT/MADT/FACP, FDT, GUDT or JSON\r\n");
    printf("\r\n");
    exit(1);
}

/**
 * The main function
 */
int main(int argc, char **argv)
{
    int i, j, k, init = 0, jsoncomm = 1;
    char *out_gudt = NULL, *out_gudm = NULL, *out_json = NULL;
    uint8_t *data;

    /* initialize */
    gudt_init();

    /* parse command line arguments */
    if(argc < 2) usage();
    for(i = 1; i < argc; i++)
        if(argv[i][0] == '-')
            for(j = k = 1; k && argv[i][j]; j++)
                switch(argv[i][j]) {
                    case 'o': out_gudt = argv[++i]; k = 0; break;
                    case 'u': out_gudm = argv[++i]; k = 0; break;
                    case 'j': out_json = argv[++i]; k = 0; jsoncomm = 1; break;
                    case 'J': out_json = argv[++i]; k = 0; jsoncomm = 0; break;
                    case 'v': verbose++; break;
                    case 'd':
                        if(!init) unitype_init();
                        unitype_dump();
                        unitype_free();
                        if(gudt_nodes) free(gudt_nodes);
                        return 0;
                    break;
                    default: usage();
                }
        else
        if((data = readfileall(argv[i]))) {
            if(!init) { init = 1; unitype_init(); }
            /* we just do a quick select here, the functions check magic bytes properly */
            switch(data[0]) {
                case '/': case '[': json_parse(data, argv[i]); break;
                case 'G': gudt_parse(data, argv[i]); break;
                case 'S': case 'D': case 'A': case 'F': aml_parse(data, argv[i]); break;
                case 0xD0: fdt_parse(data, argv[i]); break;
                default: msg("%s: unrecognized format\r\n", argv[i]); break;
            }
            free(data);
        }

    /* failsafe: sort nodes so that parent nodes always preceed children nodes, also makes output predictable */
    mem_tonodes();
    gudt_sort();

    /* generate output */
    if(gudt_hdr.hdr.numnodes) {
        if(out_gudt) gudt_packed(out_gudt); else
        if(out_gudm) gudt_unpacked(out_gudm); else
        if(out_json) json_out(out_json, jsoncomm);
        else { printf("%s", copyright); tree_out(0, 0); }
    }

    /* free resources */
    gudt_free();
    unitype_free();
    return 0;
}
