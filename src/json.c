/*
 * src/json.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief JSON format routines
 */

#include "main.h"

#include <inttypes.h>
#ifndef PRIu64
#ifdef __WIN32__
#define PRIu64 "l64u"
#define PRIx64 "l64x"
#else
#if __WORDSIZE == 32
#define PRIu64 "llu"
#define PRIx64 "llx"
#else
#define PRIu64 "lu"
#define PRIx64 "lx"
#endif
#endif
#endif

/* GUDT_T_x */
const char *types[] = {
  /* defined by GUDT */
  "DEVICE", "CPUCORE", "DMA", "IRQ", "INTC", "PINS", "LEDS", "CLOCKS", "SENSORS", "BUTTONS", "AMPER", "VOLT", "THERMAL",
  "FREQ", "L0CACHE", "L1CACHE", "L2CACHE", "L3CACHE",
  /* unassigned for now */
  "#12","#13","#14","#15","#16","#17","#18","#19","#1a","#1b","#1c","#1d","#1e","#1f","#20","#21","#22","#23","#24","#25","#26",
  "#27","#28","#29","#2a","#2b","#2c","#2d","#2e","#2f","#30","#31","#32","#33","#34","#35","#36","#37","#38","#39","#3a","#3b",
  "#3c","#3d","#3e","#3f","#40","#41","#42","#43","#44","#45","#46","#47","#48","#49","#4a","#4b","#4c","#4d","#4e","#4f","#50",
  "#51","#52","#53","#54","#55","#56","#57","#58","#59","#5a","#5b","#5c","#5d","#5e","#5f","#60","#61","#62","#63","#64","#65",
  "#66","#67","#68","#69","#6a","#6b","#6c","#6d","#6e","#6f","#70","#71","#72","#73","#74","#75","#76","#77","#78","#79","#7a",
  "#7b","#7c","#7d","#7e","#7f","#80","#81","#82","#83","#84","#85","#86","#87","#88","#89","#8a","#8b","#8c","#8d","#8e","#8f",
  "#90","#91","#92","#93","#94","#95","#96","#97","#98","#99","#9a","#9b","#9c","#9d","#9e","#9f","#a0","#a1","#a2","#a3","#a4",
  "#a5","#a6","#a7","#a8","#a9","#aa","#ab","#ac","#ad","#ae","#af","#b0","#b1","#b2","#b3","#b4","#b5","#b6","#b7","#b8","#b9",
  "#ba","#bb","#bc","#bd","#be","#bf","#c0","#c1","#c2","#c3","#c4","#c5","#c6","#c7","#c8","#c9","#ca","#cb","#cc","#cd","#ce",
  "#cf","#d0","#d1","#d2","#d3","#d4",
  "BOOT", "ROOT", "EDID", "FBPTR", "FBDIM", "MODULE", "CMDLINE", "DEFAULT", "NVSMEM", "RESVMEM", "RAM",
  /* top 32 entries must match ACPI region space */
  "MMIO", "IOPORT", "PCI", "EC", "SMB", "NVRAM", "PCIBAR", "IPMI", "GPIO", "GSB", "PCC", "#eb", "#ec","#ed","#ee","#ef",
  "#f0","#f1","#f2","#f3","#f4","#f5","#f6","#f7","#f8","#f9","#fa","#fb","#fc","#fd","#fe","#ff" };

/* GUDT_C_x, must match pci.ids */
const char *cats[] = { "UNKNOWN", "STORAGE", "NETWORK", "DISPLAY", "MULTIMEDIA", "MEMORY", "BRIDGE", "COMM", "GENERIC",
  "INPUT", "DOCK", "PROCESSOR", "SERIAL", "WIRELESS", "INTELLIGENT", "SATELLITE", "ENCRYPTION", "SIGNAL", "ACCEL", "NONESSENTIAL",
  "#14","#15","#16","#17","#18","#19","#1a","#1b","#1c","#1d","#1e","#1f","#20","#21","#22","#23","#24","#25","#26","#27","#28",
  "#29","#2a","#2b","#2c","#2d","#2e","#2f","#30","#31","#32","#33","#34","#35","#36","#37","#38","#39","#3a","#3b","#3c","#3d",
  "#3e","#3f","#40","#41","#42","#43","#44","#45","#46","#47","#48","#49","#4a","#4b","#4c","#4d","#4e","#4f","#50","#51","#52",
  "#53","#54","#55","#56","#57","#58","#59","#5a","#5b","#5c","#5d","#5e","#5f","#60","#61","#62","#63","#64","#65","#66","#67",
  "#68","#69","#6a","#6b","#6c","#6d","#6e","#6f","#70","#71","#72","#73","#74","#75","#76","#77","#78","#79","#7a","#7b","#7c",
  "#7d","#7e","#7f","#80","#81","#82","#83","#84","#85","#86","#87","#88","#89","#8a","#8b","#8c","#8d","#8e","#8f","#90","#91",
  "#92","#93","#94","#95","#96","#97","#98","#99","#9a","#9b","#9c","#9d","#9e","#9f","#a0","#a1","#a2","#a3","#a4","#a5","#a6",
  "#a7","#a8","#a9","#aa","#ab","#ac","#ad","#ae","#af","#b0","#b1","#b2","#b3","#b4","#b5","#b6","#b7","#b8","#b9","#ba","#bb",
  "#bc","#bd","#be","#bf","#c0","#c1","#c2","#c3","#c4","#c5","#c6","#c7","#c8","#c9","#ca","#cb","#cc","#cd","#ce","#cf","#d0",
  "#d1","#d2","#d3","#d4","#d5","#d6","#d7","#d8","#d9","#da","#db","#dc","#dd","#de","#df","#e0","#e1","#e2","#e3","#e4","#e5",
  "#e6","#e7","#e8","#e9","#ea","#eb","#ec","#ed","#ee","#ef","#f0","#f1","#f2","#f3","#f4","#f5","#f6","#f7","#f8","#f9","#fa",
  "#fb","#fc","#fd","#fe",
  /* pci.ids has this as mandatory "unassigned", we re-use it for the root node */
  "MACHINE" };

/* device types for root node, must match ACPI PM Profiles */
const char *chassis[] = { "UNSPECIFIED", "DESKTOP", "MOBILE", "WORKSTATION", "ENTERPRISE", "SOHO", "APPLIANCE",
  "PERFORMANCE", "TABLET" };

/* for GUDT_F_UNIT */
const char *units[] = { "byte", "word", "dword", "qword", "m128", "m256", "m512", "m1024", "m2048", "m4096", "m8192",
    "m16k", "m32k", "m64k", "m128k", "indirect" };

const char *levels[] = { "critical", "error", "warning", "info" };

static char tree_lvl[AML_MAXLEVEL], json_tmp[(ARRAY_MAXITEMS + 1) * 32];

/**
 * Search for a value in a string array
 */
int json_search(char *str, const char **list, int numlist)
{
    int i;

    for(i = 0; i < numlist; i++)
        if(!strcmp(str, list[i]))
            return i;
    return -1;
}

/**
 * Parse a JSON text
 */
void json_parse(uint8_t *ptr, char *fn)
{
    gudt_device_t *devs;
    uint64_t v, base, dataq[ARRAY_MAXITEMS];
    uint32_t i, n, num, len, parent, driver, alt, name, device, vendor, model, unit, size, datad[ARRAY_MAXITEMS];
    uint16_t dataw[ARRAY_MAXITEMS];
    uint8_t *k, *s, datab[ARRAY_MAXITEMS];
    char str[256];
    int type, category, line = 1, beg, abeg;

    /* give a chance to comment-less JSON strings too */
    if(ptr[0] == '/' && memcmp(ptr, "/* Grand Unified Device Tree */", 31)) {
err:    msg("%s: not a valid GUDT JSON\r\n", fn);
        return;
    }
    while(*ptr && *ptr != '[') ptr++;
    /* is it a one dimensional array of objects each with a type field and no nested objects? */
    if(*ptr != '[') goto err;
    for(s = ptr, type = beg = abeg = 0; *s; s++) {
        switch(*s) {
            case '[': beg++; if(beg > 2 || (beg == 2 && !abeg)) goto err; break;
            case ']': beg--; if(beg < 0) goto err; break;
            case '{': abeg++; type = 0; if(abeg > 1) goto err; break;
            case '}': abeg--; if(!type || abeg < 0) goto err; break;
        }
        if(!memcmp(s, "\"type\"", 6)) type = 1;
    }

    /* don't be too restrictive about syntax (like whitespaces and a missing comma),
     * but be very strict about keys and their values */
    while(*ptr) {
        while(*ptr && *ptr != '{') { if(*ptr == '\n') { line++; } ptr++; }
        if(!*ptr) break;
        type = -1; len = parent = driver = alt = name = device = vendor = model = unit = size = 0; base = 0; beg = line;
        memset(datab, 0, sizeof(datab)); memset(dataw, 0, sizeof(dataw)); memset(datad, 0, sizeof(datad));
        while(*ptr && *ptr != '}') {
            k = NULL; v = 0; memset(&str, 0, sizeof(str));
            /* key */
            while(*ptr && *ptr != '\"' && *ptr != '}') { if(*ptr == '\n') { line++; } ptr++; }
            if(!*ptr || *ptr == '}') break;
            for(k = ++ptr; *ptr && *ptr != '\"'; ptr++) if(*ptr == '\n') line++;
            if(!*ptr) break;
            for(ptr++; *ptr && (*ptr == ' ' || *ptr == '\t' || *ptr == ':'); ptr++) if(*ptr == '\n') line++;
            if(!*ptr) break;
            /* value */
            if(*ptr == '\"') {
                for(s = (uint8_t*)str, ptr++; *ptr && *ptr != '\"'; ptr++) {
                    if(*ptr == '\n') line++;
                    if(*ptr == '\\') { ptr++; if(*ptr == '\"') *ptr = '\''; }
                    if(*ptr >= ' ' && (uintptr_t)s - (uintptr_t)str < sizeof(str) - 1) *s++ = *ptr;
                }
                if(*ptr == '\"') ptr++;
                *s = 0;
            } else v = getint(&ptr);
            if(*ptr != '[') {
                while(*ptr && *ptr != '\"' && *ptr != ',' && *ptr != '}') { if(*ptr == '\n') { line++; } ptr++; }
                while(*ptr == ',' || *ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n')
                    { if(*ptr == '\n') { line++; } ptr++; }
            }
            /* interpret key and set corresponding variables */
            if(k) {
                if(!memcmp(k, "type", 4)) {
                    if((type = json_search(str, types, sizeof(types)/sizeof(types[0]))) == -1)
                        msg("in %s:%u invalid type '%s'\r\n", fn, line, str);
                } else
                if(!memcmp(k, "parent", 6)) {
                    devs = (gudt_device_t*)gudt_nodes;
                    num = gudt_hdr.hdr.hdrsize;
                    i = gudt_hdr.hdr.numnodes;
                    if(str[0]) {
                        n = gudt_addstr(str);
                        for(v = 0; v < gudt_hdr.hdr.numnodes && (devs[v].type != GUDT_T_DEVICE || devs[v].name != n); v++);
                        for(i = v + 1; i < gudt_hdr.hdr.numnodes && (devs[i].type != GUDT_T_DEVICE || devs[i].name != n); i++);
                    }
                    if(v >= gudt_hdr.hdr.numnodes || i < gudt_hdr.hdr.numnodes || devs[v].type != GUDT_T_DEVICE) {
                        if(gudt_hdr.hdr.numnodes) {
                            if(!str[0]) sprintf(str, "#%" PRIu64, v);
                            msg("in %s:%u %s parent node '%s'%s\r\n", fn, line,
                                i < gudt_hdr.hdr.numnodes ? "ambiguous" : "no such", str,
                                i < gudt_hdr.hdr.numnodes ? ", use node number instead of a string" : "");
                        }
                        gudt_hdr.hdr.hdrsize = num;
                        if(v >= gudt_hdr.hdr.numnodes) v = 0;
                    }
                    parent = v;
                } else
                if(!memcmp(k, "category", 8)) {
                    if((category = json_search(str, cats, sizeof(cats)/sizeof(cats[0]))) == -1)
                        { category = 0; msg("in %s:%u invalid category '%s'\r\n", fn, line, str); }
                    if(category > 255) { device = category - 255; category = 255; }
                } else
                if(!memcmp(k, "driver", 6)) {
                    if(!str[0]) msg("in %s:%u invalid driver\r\n", fn, line);
                    else driver = gudt_addstr(str);
                } else
                if(!memcmp(k, "alternative", 11)) {
                    if(str[0]) alt = gudt_addstr(str);
                } else
                if(!memcmp(k, "name", 4)) {
                    if(!str[0]) msg("in %s:%u invalid name\r\n", fn, line);
                    else name = gudt_addstr(str);
                } else
                if(!memcmp(k, "unit", 4)) {
                    if((unit = json_search(str, units, sizeof(units)/sizeof(units[0]))) == -1U)
                        { unit = 0; msg("in %s:%u invalid unit '%s'\r\n", fn, line, str); }
                } else
                if(!memcmp(k, "level", 5)) {
                    if((size = json_search(str, units, sizeof(levels)/sizeof(levels[0]))) == -1U)
                        { size = 0; msg("in %s:%u invalid level '%s'\r\n", fn, line, str); }
                } else
                if(!memcmp(k, "data", 4)) {
                    if(*ptr != '[') msg("in %s:%u no array for inlined data\r\n", fn, line);
                    else {
                        while(*ptr == '[' || *ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n')
                            { if(*ptr == '\n') { line++; } ptr++; }
                        for(s = ptr, abeg = line; *ptr && *ptr != ']' && *ptr != '}'; ptr++) if(*ptr == '\n') line++;
                        if(unit > 3) {
                            msg("in %s:%u invalid unit '%s' for inlined data\r\n", fn, abeg, units[unit]);
                            unit = len = 0;
                        } else {
                            for(len = 0; s < ptr;) {
                                if(len == ARRAY_MAXITEMS) {
                                    msg("in %s:%u too many %s elements for inlined data (max %u)\r\n",
                                        fn, abeg, units[unit], ARRAY_MAXITEMS);
                                    break;
                                }
                                v = getint(&s);
                                if(v >> (8<<unit))
                                    msg("in %s:%u %u%s %s element 0x%" PRIx64 " will be truncated!\r\n",
                                        fn, abeg, len + 1, !len ? "st" : (len == 1 ? "nd" : (len == 2 ? "rd" : "th")), units[unit],
                                        v);
                                switch(unit) {
                                    case 0: datab[len++] = v; break;
                                    case 1: dataw[len++] = v; break;
                                    case 2: datad[len++] = v; break;
                                    case 3: dataq[len++] = v; break;
                                }
                                for(; s < ptr && (*s == ',' || *s == ' ' || *s == '\t' || *s == '\r' || *s == '\n'); s++)
                                    if(*s == '\n') abeg++;
                            }
                        }
                    }
                } else
                if(!memcmp(k, "device", 6)) {
                    if(!str[0]) device = v; else
                    if((device = json_search(str, chassis, sizeof(chassis)/sizeof(chassis[0]))) == -1U)
                        { device = 0; msg("in %s:%u invalid device '%s'\r\n", fn, line, str); }
                } else
                if(!memcmp(k, "vendor", 6)) {
                    if(str[0]) vendor = unitype_pciids_by_name(0, str);
                    else vendor = v;
                } else
                if(!memcmp(k, "model", 5)) {
                    if(str[0]) {
                        unit = unitype_pciids_by_name(0, str);
                        vendor = unit >> 16; model = unit & 0xffff;
                    } else
                        model = v;
                } else
                if(!memcmp(k, "base", 4) || !memcmp(k, "mask", 4)) base = v; else
                if(!memcmp(k, "node", 4)) {
                    devs = (gudt_device_t*)gudt_nodes;
                    num = gudt_hdr.hdr.hdrsize;
                    i = gudt_hdr.hdr.numnodes;
                    if(type == GUDT_T_INTC && str[0]) {
                        n = gudt_addstr(str);
                        for(v = 0; v < gudt_hdr.hdr.numnodes && (devs[v].type != GUDT_T_DEVICE || devs[v].name != n); v++);
                        for(i = v + 1; i < gudt_hdr.hdr.numnodes && (devs[i].type != GUDT_T_DEVICE || devs[i].name != n); i++);
                    }
                    if(v >= gudt_hdr.hdr.numnodes || i < gudt_hdr.hdr.numnodes ||
                      (type == GUDT_T_INTC && devs[v].type != GUDT_T_DEVICE) ||
                      (type != GUDT_T_INTC && devs[v].type == GUDT_T_DEVICE)) {
                        if(!str[0]) sprintf(str, "#%" PRIu64, v);
                        msg("in %s:%u %s node '%s'%s\r\n", fn, line,
                            i < gudt_hdr.hdr.numnodes ? "ambiguous" : "not a valid", str,
                            i < gudt_hdr.hdr.numnodes ? ", use node number instead of a string" : "");
                        gudt_hdr.hdr.hdrsize = num;
                        if(v >= gudt_hdr.hdr.numnodes) v = 0;
                    }
                    if(type != GUDT_T_INTC) unit = GUDT_F_INDIRECT;
                    size = v;
                } else
                if(!memcmp(k, "size", 4) || !memcmp(k, "affinity", 8) || !memcmp(k, "sn", 2) || !memcmp(k, "div", 3)) size = v;
                else {
                    for(s = (uint8_t*)str; *k && *k != '\"' && *k > ' ' && (uintptr_t)s - (uintptr_t)str < sizeof(str) - 1;)
                        *s++ = *k++;
                    *s = 0;
                    msg("in %s:%u unrecognized JSON key '%s'\r\n", fn, line, str);
                }
            }
        }
        /* end of block, let's see if we got all the required fields, and if so, add the node */
        if(type == -1) msg("in %s:%u missing type for node\r\n", fn, beg);
        if(type == GUDT_T_DEVICE) {
            /* device type nodes */
            if(!name) msg("in %s:%u missing name for node\r\n", fn, beg); else
            if(!driver) msg("in %s:%u missing driver for node\r\n", fn, beg); else
                gudt_add(name, driver, alt, parent, category, type, device, (vendor << 16) | (model & 0xffff));
        } else
        /* root node might have a memory map, in which case parent is 0 */
        if(!parent && (type < GUDT_T_NVSMEM || type > GUDT_T_RESVMEM))
            msg("in %s:%u missing parent for node\r\n", fn, beg);
        else {
            /* resource type nodes */
            devs = (gudt_device_t*)gudt_nodes;
            if(len)
                gudt_add(devs[parent].name, devs[parent].driver, 0, parent, 0x10 | unit, type, unit == 0 ? (uintptr_t)&datab :
                    (unit == 1 ? (uintptr_t)&dataw : (unit == 2 ? (uintptr_t)&datad : (uintptr_t)&dataq)), len);
            else
                gudt_add(devs[parent].name, devs[parent].driver, 0, parent, unit, type, base, size);
        }
    }
}

/**
 * Pretty print node data
 */
char *json_node(gudt_node_t *node)
{
    int i, o = 0, u = GUDT_F_UNIT(node->flags), l = GUDT_F_DATA(node->flags);

    if(l) {
        /* it's inlined data */
        json_tmp[o++] = '[';
        for(i = 0; i < l; i++) {
            if(i) o += snprintf(json_tmp + o, sizeof(json_tmp) - o, ",");
            switch(u) {
                case 1: o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "0x%02x", node->r.b.data[i]); break;
                case 2: o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "0x%04x", node->r.w.data[i]); break;
                case 4: o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "0x%08x", node->r.d.data[i]); break;
                case 8: o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "0x%016" PRIx64, node->r.q.data[i]); break;
            }
        }
        json_tmp[o++] = ']'; json_tmp[o] = 0;
    } else
    if(node->flags == GUDT_F_INDIRECT) {
        /* indirect node reference */
        o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "node #%u", node->r.p.size);
        if(node->r.p.base)
            o += snprintf(json_tmp + o, sizeof(json_tmp) - o, " & 0x%08x_%08x",
                (uint32_t)(node->r.p.base >> 32), (uint32_t)node->r.p.base);
    } else {
        /* pointed resource */
        o += snprintf(json_tmp + o, sizeof(json_tmp) - o, "0x%08x_%08x %s%u",
            (uint32_t)(node->r.p.base >> 32), (uint32_t)node->r.p.base,
            node->type == GUDT_T_FREQ ? "div " : (node->type == GUDT_T_INTC ? "node #" : (node->type == GUDT_T_CPUCORE ? "sn " : "")),
            node->r.p.size);
        if(node->type == GUDT_T_INTC && node->r.p.size < gudt_hdr.hdr.numnodes)
            o += snprintf(json_tmp + o, sizeof(json_tmp) - o, " \"%s\"",
                gudt_hdr.strs + ((gudt_device_t*)&gudt_nodes[node->r.p.size])->name);
        if(node->flags) o += snprintf(json_tmp + o, sizeof(json_tmp) - o, " (%s aligned)", units[node->flags & 15]);
    }
    return json_tmp;
}

/**
 * Output JSON text
 */
void json_out(char *fn, int comments)
{
    char *buf = NULL;
    gudt_device_t *dev, *devs = (gudt_device_t*)gudt_nodes;
    int i, j, n, o = 0, l = 0;

    for(i = 0; i < (int)gudt_hdr.hdr.numnodes; i++) {
        if(o + 4096 > l) {
            l += 65536;
            buf = (char*)realloc(buf, l);
            if(!buf) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
            if(!o) {
                if(comments) o += snprintf(buf + o, l - o, "/* Grand Unified Device Tree */\r\n");
                o += snprintf(buf + o, l - o, "[\r\n");
            }
        }
        o += snprintf(buf + o, l - o, "  {\r\n");
        if(comments) {
            o += snprintf(buf + o, l - o, "    /* #%u", i);
            if(gudt_nodes[i].type == GUDT_T_DEVICE) o += snprintf(buf + o, l - o, ", %s", unitype_out((gudt_device_t*)&gudt_nodes[i]));
            o += snprintf(buf + o, l - o, " */\r\n");
        }
        o += snprintf(buf + o, l - o, "    \"type\": \"%s\",\r\n", types[gudt_nodes[i].type]);
        if(gudt_nodes[i].parent) {
            dev = (gudt_device_t*)&gudt_nodes[gudt_nodes[i].parent];
            if(dev->type == GUDT_T_DEVICE && dev->name >= 8 && dev->name < gudt_hdr.hdr.hdrsize) {
                for(j = n = 0; j < (int)gudt_hdr.hdr.numnodes; j++)
                    if(devs[j].type == GUDT_T_DEVICE && devs[j].name == dev->name) n++;
                if(n == 1)
                    o += snprintf(buf + o, l - o, "    \"parent\": \"%s\",\r\n", gudt_hdr.strs + dev->name);
                else
                    o += snprintf(buf + o, l - o, "    \"parent\": %u,\r\n", gudt_nodes[i].parent);
            } else
                o += snprintf(buf + o, l - o, "    \"parent\": %u,\r\n", gudt_nodes[i].parent);
        }
        if(gudt_nodes[i].type == GUDT_T_DEVICE) {
            /* device type nodes */
            dev = (gudt_device_t*)&gudt_nodes[i];
            o += snprintf(buf + o, l - o, "    \"category\": \"%s\",\r\n", cats[dev->category]);
            o += snprintf(buf + o, l - o, "    \"driver\": \"%s\",\r\n", dev->driver >= 8 && dev->driver < gudt_hdr.hdr.hdrsize ?
                gudt_hdr.strs + dev->driver : "");
            if(dev->alternative >= 8 && dev->alternative < gudt_hdr.hdr.hdrsize)
                o += snprintf(buf + o, l - o, "    \"alternative\": \"%s\",\r\n", gudt_hdr.strs + dev->alternative);
            o += snprintf(buf + o, l - o, "    \"name\": \"%s\",\r\n",
                dev->name >= 8 && dev->name < gudt_hdr.hdr.hdrsize ? gudt_hdr.strs + dev->name : "");
            if(dev->category == GUDT_C_MACHINE && dev->device < sizeof(chassis)/sizeof(chassis[0]))
                o += snprintf(buf + o, l - o, "    \"device\": \"%s\",\r\n", chassis[dev->device]);
            else
                o += snprintf(buf + o, l - o, "    \"device\": 0x%04x,\r\n", dev->device);
            o += snprintf(buf + o, l - o, "    \"vendor\": 0x%04x,\r\n", dev->vendor);
            o += snprintf(buf + o, l - o, "    \"model\": 0x%04x\r\n", dev->model);
        } else {
            /* resource type nodes */
            if(gudt_nodes[i].flags)
                o += snprintf(buf + o, l - o, "    \"unit\": \"%s\",\r\n", units[gudt_nodes[i].flags & 15]);
            if(GUDT_F_DATA(gudt_nodes[i].flags)) {
                o += snprintf(buf + o, l - o, "    \"data\": %s\r\n", json_node(&gudt_nodes[i]));
            } else {
                /* base field */
                if(gudt_nodes[i].flags != GUDT_F_INDIRECT)
                    o += snprintf(buf + o, l - o, "    \"base\": 0x%" PRIx64 "%s,\r\n", gudt_nodes[i].r.p.base,
                        !comments ? "" : (
                        gudt_nodes[i].type == GUDT_T_AMPER ? " /* mA */" : (
                        gudt_nodes[i].type == GUDT_T_VOLT ? " /* mV */" : (
                        gudt_nodes[i].type == GUDT_T_THERMAL ? " /* C° */" : (
                        gudt_nodes[i].type == GUDT_T_FREQ ? " /* mHz */" : "")))));
                else if(gudt_nodes[i].r.p.base)
                    o += snprintf(buf + o, l - o, "    \"mask\": 0x%" PRIx64 ",\r\n", gudt_nodes[i].r.p.base);
                /* size field */
                dev = (gudt_device_t*)&gudt_nodes[gudt_nodes[i].r.p.size];
                if(gudt_nodes[i].type == GUDT_T_THERMAL)
                    o += snprintf(buf + o, l - o, "    \"level\": \"%s\"\r\n", levels[gudt_nodes[i].r.p.size & 3]);
                else
                if(gudt_nodes[i].type == GUDT_T_INTC && gudt_nodes[i].r.p.size < gudt_hdr.hdr.numnodes &&
                  dev->type == GUDT_T_DEVICE && dev->name >= 8 && dev->name < gudt_hdr.hdr.hdrsize) {
                    for(j = n = 0; j < (int)gudt_hdr.hdr.numnodes; j++)
                        if(devs[j].type == GUDT_T_DEVICE && devs[j].name == dev->name) n++;
                    if(n == 1)
                        o += snprintf(buf + o, l - o, "    \"node\": \"%s\"\r\n", gudt_hdr.strs + dev->name);
                    else
                        o += snprintf(buf + o, l - o, "    \"node\": %u\r\n", gudt_nodes[i].r.p.size);
                } else
                    o += snprintf(buf + o, l - o, "    \"%s\": %u\r\n",
                        gudt_nodes[i].type == GUDT_T_FREQ ? "div" : (gudt_nodes[i].type == GUDT_T_CPUCORE ? "sn" :
                        (gudt_nodes[i].type == GUDT_T_INTC || gudt_nodes[i].flags == GUDT_F_INDIRECT ? "node" :
                        (gudt_nodes[i].type == GUDT_T_IRQ ? "affinity" : "size"))),
                        gudt_nodes[i].r.p.size);
            }
        }
        o += snprintf(buf + o, l - o, "  }%s\r\n", i + 1 < (int)gudt_hdr.hdr.numnodes ? "," : "");
    }
    o += snprintf(buf + o, l - o, "]\r\n");
    writefileall(fn, (uint8_t*)buf, o);
    free(buf);
}

/**
 * Display the nodes in a tree
 */
void tree_out(uint16_t idx, int lvl)
{
    gudt_device_t *dev = (gudt_device_t*)&gudt_nodes[idx];
    int i, c, n;

    if(idx >= gudt_hdr.hdr.numnodes || lvl >= AML_MAXLEVEL) return;
    /* display current record */
    printf("  ");
    for(i = 0; i < lvl; i++)
        printf("%c%c ", tree_lvl[i] == '`' && i + 1 < lvl ? ' ' : tree_lvl[i], tree_lvl[i] == ' ' || i + 1 < lvl ? ' ' : '-');
    if(gudt_nodes[idx].type == GUDT_T_DEVICE) {
        /* device node */
        printf("#%03u %-6s ", idx, types[gudt_nodes[idx].type]);
        printf("%s(%02x.%02x) ", cats[dev->category], dev->category, dev->device);
        printf("\"%s\" ", dev->driver >= 8 && dev->driver < gudt_hdr.hdr.hdrsize ? gudt_hdr.strs + dev->driver :
          (dev->alternative >= 8 && dev->alternative < gudt_hdr.hdr.hdrsize ? gudt_hdr.strs + dev->alternative : "?"));
        printf("\"%s\"", dev->name >= 8 && dev->name < gudt_hdr.hdr.hdrsize ? gudt_hdr.strs + dev->name : "?");
        printf(" (%s)", unitype_out(dev));
    } else {
        /* resource node */
        printf("%-10s%s%s", types[gudt_nodes[idx].type], GUDT_F_DATA(gudt_nodes[idx].flags) ? units[gudt_nodes[idx].flags & 15] : "",
            json_node(&gudt_nodes[idx]));
    }
    printf("\r\n");
    /* count children */
    for(i = n = 0; i < (int)gudt_hdr.hdr.numnodes; i++)
        if(i != idx && gudt_nodes[i].parent == idx) n++;
    /* display children */
    for(i = c = 0; i < (int)gudt_hdr.hdr.numnodes; i++)
        if(i != idx && gudt_nodes[i].parent == idx) {
            tree_lvl[lvl] = c + 1 == n ? '`' : (c < n ? '|' : ' ');
            tree_out(i, lvl + 1);
            c++;
        }
}
