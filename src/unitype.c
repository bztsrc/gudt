/*
 * src/unitype.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Universal Device Type routines
 */

#include "main.h"

#ifdef __WIN32__
#define DB_PATH "C:\\Windows\\System32\\"
#else
#define DB_PATH "/usr/share/hwdata/"
#endif

/* gudt.ids, unitype database, see https://gitlab.com/bztsrc/gudt/raw/main/gudt.ids */
typedef struct {
    uint8_t category;
    uint16_t device;
    uint16_t vendor;
    uint16_t model;
    char *magic;
} unitype_t;
unitype_t *unitype = NULL;
int numunitype = 0;

/*** normally all we need is the gudt.ids, but if we have more databases, try to use those to expand ours ***/

/* pci.ids, PCI database, see https://pci-ids.ucw.cz/v2.2/pci.ids */
pciids_t **vendors = NULL, **models = NULL, **devices = NULL;
int numvendors = 0, nummodels = 0, numdevices = 0, nodb = 0;

/* devids.txt, EISA database, see https://uefi.org/PNP_ACPI_Registry and
 * https://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/devids.txt */
pciids_t devidsmagic[] = {
    { 0x080000, 0, "interrupt controller" },{ 0x080002, 0, "timer" },               { 0x080001, 0, "dma" },
    { 0x090000, 0, "keyboard" },            { 0x090080, 0, "input" },               { 0x070001, 0, "parallel" },
    { 0x070001, 0, "printer" },             { 0x070001, 0, "lpt port" },            { 0x070000, 0, "serial device" },
    { 0x070000, 0, "serial controller" },   { 0x070001, 0, "com port" },            { 0x010000, 0, "disk" },
    { 0x010000, 0, "storage" },             { 0x030000, 0, "display" },             { 0x030000, 0, "vga" },
    { 0x030000, 0, "xga" },                 { 0x030000, 0, "mga" },                 { 0x030000, 0, "ati" },
    { 0x030000, 0, "vesa" },                { 0x080080, 0, "apic" },                { 0x060000, 0, "peripheral bus" },
    { 0x060002, 0, "eisa bus" },            { 0x060001, 0, "isa bus" },             { 0x060003, 0, "mca bus" },
    { 0x040003, 0, "speaker" },             { 0x040003, 0, "sound" },               { 0x040001, 0, "synth" },
    { 0x040001, 0, "music" },               { 0x040000, 0, "video" },               { 0x040001, 0, "audio" },
    { 0x080003, 0, "cmos" },                { 0x080080, 0, "bios" },                { 0x080080, 0, "board" },
    { 0x080080, 0, "acpi" },                { 0x040040, 0, "coprocessor" },         { 0x0c0003, 0, "uhci" },
    { 0x0c0003, 0, "ohci" },                { 0x0c0003, 0, "ehci" },                { 0x0c0003, 0, "xhci" },
    { 0x070002, 0, "multifunction" },       { 0x0c0003, 0, "usb controller" },      { 0x080005, 0, "sd host" },
    { 0x080080, 0, "power" },               { 0x060005, 0, "pcmcia" },              { 0x090002, 0, "mice" },
    { 0x090002, 0, "mouse" },               { 0x090003, 0, "scanner" },             { 0x090004, 0, "gamepad" },
    { 0x090004, 0, "game port" },           { 0x090004, 0, "joystick" },            { 0x020000, 0, "network" },
    { 0x020000, 0, "ether" },               { 0x020000, 0, "arcnet" },              { 0x020000, 0, "nic" },
    { 0x020000, 0, "lan adapter" },         { 0x020001, 0, "token ring" },          { 0x020001, 0, "tokenlink" },
    { 0x020002, 0, "fddi" },                { 0x020007, 0, "infiniband" },          { 0x010000, 0, "scsi" },
    { 0x010008, 0, "sata" },                { 0x010008, 0, "cd rom" },              { 0x040001, 0, "sound video capture" },
    { 0x070003, 0, "modem" }
};

extern const char *chassis[];
char unitype_tmp[1024];

/**
 * Comparator
 */
int utcmp(const void *a, const void *b)
{
    return strcmp(((unitype_t*)a)->magic, ((unitype_t*)b)->magic);
}

/**
 * Comparator
 */
int pcicmp(const void *a, const void *b)
{
    return (*((pciids_t**)a))->id  - (*((pciids_t**)b))->id;
}

/**
 * Convert hex string into integer
 */
uint64_t gethex(uint8_t **s)
{
    uint64_t ret = 0;
    uint8_t c;

    for(; **s; (*s)++) {
        c = **s;
        if(c >= '0' && c <= '9') {      ret <<= 4; ret += (uint64_t)(c-'0'); }
        else if(c >= 'a' && c <= 'f') { ret <<= 4; ret += (uint64_t)(c-'a'+10); }
        else if(c >= 'A' && c <= 'F') { ret <<= 4; ret += (uint64_t)(c-'A'+10); }
        else if(c != '_') break;
    }
    while(**s && (**s == ' ' || **s == '\t')) (*s)++;
    return ret;
}

/**
 * Convert decimal string into integer
 */
uint64_t getint(uint8_t **s)
{
    uint32_t ret = 0;

    if(s[0][0] == '0' && s[0][1] == 'x') { (*s) += 2; return gethex(s); }
    for(; **s; (*s)++) {
        if(**s >= '0' && **s <= '9') { ret *= 10; ret += (uint64_t)(**s-'0'); }
        else break;
    }
    while(**s && (**s == ' ' || **s == '\t')) (*s)++;
    return ret;
}

/**
 * Initialize Universal Device Type database
 * If we can find the other databases, use them to expand our data
 */
void unitype_init(void)
{
    uint32_t vendor, device, category = 0;
    uint8_t *data, *s, *e;
    int i;
#ifdef DEBUG
    int l, f = 0, n = 0;
    char tmp[256], tmp2[256], *d;
#endif

    /* this is a reliable, easy to parse text file, with well-trusted data. Use it as a baseline */
    if((data = readfileall(DB_PATH "pci.ids")) || (data = readfileall("pci.ids"))) {
        vendor = category = 0;
        for(s = data; *s; s = e) {
            while(*s && (*s == '\r' || *s == '\n' || *s == ' ')) s++;
            if(!*s) break;
            for(e = s; *e && *e != '\r' && *e != '\n'; e++) if(*e == '\"') *e = '\'';
            /* comments and two tabs: sub-device or sub-sub-class */
            if(*s == '#' || (s[0] == '\t' && s[1] == '\t')) continue;
            /* classes */
            if(s[0] == 'C' && s[1] == ' ') { s += 2; category = gethex(&s); vendor = 0; } else
            /* no tab: vendor (deliberately do not read 0) */
            if(s[0] != '\t') {
                category = 0; vendor = gethex(&s);
                if(vendor) {
                    i = numvendors++;
                    vendors = (pciids_t**)realloc(vendors, numvendors * sizeof(pciids_t*));
                    if(!vendors) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                    vendors[i] = malloc(sizeof(pciids_t) + e - s);
                    if(!vendors[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                    vendors[i]->id = vendor;
                    memcpy(vendors[i]->name, s, e - s);
                    vendors[i]->name[e - s] = 0;
                    vendors[i]->patched = 0;
                }
            } else
            /* single tab: device */
            if(s[0] == '\t' && !category && vendor) {
                s++; device = gethex(&s);
                i = nummodels++;
                models = (pciids_t**)realloc(models, nummodels * sizeof(pciids_t*));
                if(!models) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                models[i] = malloc(sizeof(pciids_t) + e - s);
                if(!models[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                models[i]->id = (vendor << 16) | (device & 0xffff);
                memcpy(models[i]->name, s, e - s);
                models[i]->name[e - s] = 0;
                models[i]->patched = 0;
            } else
            /* single tab: sub-class. Deliberately do not read 0, 0xff and class 0x40 being a co-processor is a mistake
             * see https://pcisig.com/sites/default/files/files/PCI_Code-ID_r_1_12__v9_Jan_2020.pdf */
            if(s[0] == '\t' && category && category != 0x40 && category != 0xff) {
                s++; device = gethex(&s);
                i = numdevices++;
                devices = (pciids_t**)realloc(devices, numdevices * sizeof(pciids_t*));
                if(!devices) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                devices[i] = (pciids_t*)malloc(sizeof(pciids_t) + e - s);
                if(!devices[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                devices[i]->id = (category << 16) | (device & 0xffff);
                memcpy(devices[i]->name, s, e - s);
                devices[i]->name[e - s] = 0;
                devices[i]->patched = 0;
            }
        }
        free(data);
    }

    /* read in our own database */
    if((data = readfileall(DB_PATH "gudt.ids")) || (data = readfileall("../gudt.ids")) || (data = readfileall("gudt.ids"))) {
        for(s = data; *s; s = e) {
            while(*s && (*s == '\r' || *s == '\n' || *s == ' ' || *s == '\t')) s++;
            if(!*s) break;
            for(e = s; *e && *e != '\r' && *e != '\n'; e++) if(*e == '\"') *e = '\'';
            /* comments */
            if(*s == '#') continue;
            /* patch categories */
            if(s[0] == 'C' && s[1] == ' ') {
                s += 2; category = gethex(&s); device = gethex(&s);
                if(category) {
                    for(i = 0; i < numdevices && devices[i]->id != ((category << 16) | (device & 0xffff)); i++);
                    if(i >= numdevices) {
                        i = numdevices++;
                        devices = (pciids_t**)realloc(devices, numdevices * sizeof(pciids_t*));
                        if(!devices) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        devices[i] = NULL;
                    }
                    if(!devices[i] || memcmp(devices[i]->name, s, e - s) || devices[i]->name[e - s]) {
                        devices[i] = (pciids_t*)realloc(devices[i], sizeof(pciids_t) + e - s);
                        if(!devices[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        devices[i]->id = ((category & 0xff) << 16) | (device & 0xffff);
                        memcpy(devices[i]->name, s, e - s);
                        devices[i]->name[e - s] = 0;
                        devices[i]->patched = 1;
                    }
                }
            } else
            /* patch vendor */
            if(s[0] == 'V' && s[1] == ' ') {
                s += 2; vendor = gethex(&s);
                if(vendor) {
                    for(i = 0; i < numvendors && vendors[i]->id != vendor; i++);
                    if(i >= numvendors) {
                        i = numvendors++;
                        vendors = (pciids_t**)realloc(vendors, numvendors * sizeof(pciids_t*));
                        if(!vendors) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        vendors[i] = NULL;
                    }
                    if(!vendors[i] || memcmp(vendors[i]->name, s, e - s) || vendors[i]->name[e - s]) {
                        vendors[i] = (pciids_t*)realloc(vendors[i], sizeof(pciids_t) + e - s);
                        if(!vendors[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        vendors[i]->id = vendor;
                        memcpy(vendors[i]->name, s, e - s);
                        vendors[i]->name[e - s] = 0;
                        vendors[i]->patched = 1;
                    }
                }
            } else
            /* patch model */
            if(s[0] == 'M' && s[1] == ' ') {
                s += 2; vendor = gethex(&s); device = gethex(&s);
                /* vendor id can't be 0, but deviceid could be */
                if(vendor) {
                    for(i = 0; i < nummodels && models[i]->id != ((vendor << 16) | (device & 0xffff)); i++);
                    if(i >= nummodels) {
                        i = nummodels++;
                        models = (pciids_t**)realloc(models, nummodels * sizeof(pciids_t*));
                        if(!models) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        models[i] = NULL;
                    }
                    if(!models[i] || memcmp(models[i]->name, s, e - s) || models[i]->name[e - s]) {
                        models[i] = (pciids_t*)realloc(models[i], sizeof(pciids_t) + e - s);
                        if(!models[i]) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        models[i]->id = (vendor << 16) | (device & 0xffff);
                        memcpy(models[i]->name, s, e - s);
                        models[i]->name[e - s] = 0;
                        models[i]->patched = 1;
                    }
                }
            } else {
                /* pair of device type - driver/name magic bytes */
                i = numunitype++;
                unitype = (unitype_t*)realloc(unitype, numunitype * sizeof(unitype_t));
                if(!unitype) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                memset(&unitype[i], 0, sizeof(unitype_t));
                unitype[i].category = gethex(&s);
                unitype[i].device = gethex(&s);
                unitype[i].vendor = gethex(&s);
                unitype[i].model = gethex(&s);
                if(!(unitype[i].magic = (char*)malloc(e - s + 1))) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                memcpy(unitype[i].magic, s, e - s); /* <- this is the actual key */
                unitype[i].magic[e - s] = 0;
            }
        }
        free(data);
    }

#ifdef DEBUG
    /* this is a mess, difficult to parse, most likely doesn't even exists. But if it do exists, let's try to read it in and
     * update our database with its records. Always check its results manually!!! Because ACPI specification requires it, we
     * can know for sure that a pcisig vendorid and modelid that we use exists for each and every PNP ID / ACPI ID string,
     * but unfortunatelly they don't tell us, so we must figure out somehow which one is which */
    if((data = readfileall(DB_PATH "devids.txt")) || (data = readfileall("devids.txt"))) {
        for(s = data; *s; s = e) {
            while(*s && (*s == '\r' || *s == '\n' || *s == ' ' || *s == '\t')) s++;
            if(!*s) break;
            for(e = s; *e && *e != '\r' && *e != '\n'; e++) if(*e == '\"') *e = '\'';
            /* try to find the first category */
            if(!f) {
                if(s[0] != '*' || s[1] != '*') continue;
                f = 1;
            }
            /* end of list */
            if(s[0] == '=' && s[1] == '=') break;
            /* stupid padding for comments */
            if(*s == ' ' || *s == '\t') continue;
            /* category */
            if(*s == '*' || *s == '-') {
                while(s < e && (*s == '*' || *s == '-' || *s == ' ')) s++;
                category = 0;
                for(d = tmp, i = 0; (s + i) < e; d++, i++)
                    *d = s[i] >= 'A' && s[i] <= 'Z' ? s[i] + 'a' - 'A' : s[i];
                *d = 0;
                /* try to match category name and set a default category code */
                for(i = 0; i < (int)(sizeof(devidsmagic)/sizeof(devidsmagic[0])); i++) {
                    l = strlen(devidsmagic[i].name);
                    if(!memcmp(devidsmagic[i].name, s, l)) { category = devidsmagic[i].id; break; }
                }
            } else
            if(*s >= 'A' && *s <= 'Z' && s[15] == ' ' && s[16] != ' ') {
                /* hopefully a device... */
                for(l = 0; s[l] != ' '; l++);
                for(i = 0; i < numunitype && !(!memcmp(s, unitype[i].magic, l) && !unitype[i].magic[l]); i++);
                if(i >= numunitype) {
                    /* this PNP ID / ACPI ID isn't in our database yet */
                    for(d = tmp, i = 16; (s + i) < e && i < 254; d++, i++)
                        *d = s[i] >= 'A' && s[i] <= 'Z' ? s[i] + 'a' - 'A' : ((s[i] >= 'a' && s[i] <= 'z') ||
                            (s[i] >= '0' && s[i] <= '9') ? s[i] : ' ');
                    *d = 0;
                    /* start with the default category code and override it if we have a match */
                    for(device = category, i = 0; i < (int)(sizeof(devidsmagic)/sizeof(devidsmagic[0])); i++)
                        if(strstr(tmp, devidsmagic[i].name)) { device = devidsmagic[i].id; break; }
                    if(device) {
                        /* let's try to find the model
                         * (chances are low that the two database's name strings actually match, but it's great when they do) */
                        for(vendor = 0, i = 0; i < nummodels; i++) {
                            for(d = tmp2, l = 0; l < 254; d++, l++)
                                *d = models[i]->name[l] >= 'A' && models[i]->name[l] <= 'Z' ? models[i]->name[l] + 'a' - 'A' : (
                                    (models[i]->name[l] >= 'a' && models[i]->name[l] <= 'z') ||
                                    (models[i]->name[l] >= '0' && models[i]->name[l] <= '9') ? models[i]->name[l] : ' ');
                            *d = 0;
                            if(l > e - (s + 16)) l = e - (s + 16);
                            if(!memcmp(tmp, tmp2, l)) { vendor = models[i]->id; break; }
                        }
                        /* let's try to find the vendor at least */
                        if(!vendor)
                            for(i = 0; i < numvendors; i++) {
                                for(d = tmp2, l = 0; l < 254 && (
                                    (vendors[i]->name[l] >= 'A' && vendors[i]->name[l] <= 'Z') ||
                                    (vendors[i]->name[l] >= 'a' && vendors[i]->name[l] <= 'z') ||
                                    (vendors[i]->name[l] >= '0' && vendors[i]->name[l] <= '9')); d++, l++)
                                        *d = vendors[i]->name[l] >= 'A' && vendors[i]->name[l] <= 'Z' ?
                                            vendors[i]->name[l] + 'a' - 'A' : vendors[i]->name[l];
                                *d = 0;
                                if(strstr(tmp, tmp2)) { vendor = vendors[i]->id << 16; break; }
                            }
                        /* pair of device type - driver/name magic bytes */
                        i = numunitype++;
                        unitype = (unitype_t*)realloc(unitype, numunitype * sizeof(unitype_t));
                        if(!unitype) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        memset(&unitype[i], 0, sizeof(unitype_t));
                        unitype[i].category = (device >> 16) & 0xff;
                        unitype[i].device = (device & 0xffff);
                        unitype[i].vendor = (vendor >> 16);
                        unitype[i].model = (vendor & 0xffff);
                        for(l = 0; s[l] != ' '; l++);
                        if(!(unitype[i].magic = (char*)malloc(l + 1))) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
                        memcpy(unitype[i].magic, s, l);
                        unitype[i].magic[l] = 0;
                        if(verbose)
                            msg("devids.txt: adding %02x %02x %04x %04x %s\r\n", unitype[i].category,
                                unitype[i].device, unitype[i].vendor, unitype[i].model, unitype[i].magic);

                        n++;
                    }
                }
            }
        }
        free(data);
        if(verbose) msg("devids.txt: added %u more record(s)\r\n", n);
    }
#endif

    /* sort the tables so that we can use O(log2) search on them */
    if(vendors) qsort(vendors, numvendors, sizeof(pciids_t*), pcicmp);
    if(models)  qsort(models,  nummodels,  sizeof(pciids_t*), pcicmp);
    if(devices) qsort(devices, numdevices, sizeof(pciids_t*), pcicmp);
    if(unitype) qsort(unitype, numunitype, sizeof(unitype_t), utcmp);

    if(verbose)
        msg("numunitype %u, numdevices %u, numvendors %u, nummodels %u\r\n",
            numunitype, numdevices, numvendors, nummodels);
}

/**
 * Look up a record
 */
int unitype_lookup(char *magic)
{
    int s, e, h, i, l;

    if(unitype && magic && *magic) {
        for(l = 0; magic[l] && magic[l] != '@'; l++);
        s = 0; e = numunitype - 1;
        while(s <= e) {
            h = (e + s) >> 1;
            i = memcmp(unitype[h].magic, magic, l);
            if(!i && unitype[h].magic[l]) i = 1;
            if(!i) return h; else if(i > 0) e = h - 1; else s = h + 1;
        }
    }
    return -1;
}

/**
 * Fill in device node fields
 */
void unitype_node(gudt_device_t *dev)
{
    int i;

    /* make a guess based on device's name */
    if((dev->name && (i = unitype_lookup(gudt_hdr.strs + dev->name)) != -1)) {
        if(unitype[i].category && dev->category != GUDT_C_MACHINE) dev->category = unitype[i].category;
        if(unitype[i].device) dev->device = unitype[i].device;
        if(unitype[i].vendor) dev->vendor = unitype[i].vendor;
        if(unitype[i].model)  dev->model = unitype[i].model;
    }
    /* overwrite fields if there's a driver name match */
    if((dev->driver && (i = unitype_lookup(gudt_hdr.strs + dev->driver)) != -1) ||
       (dev->alternative && (i = unitype_lookup(gudt_hdr.strs + dev->alternative)) != -1)) {
        if(unitype[i].category && dev->category != GUDT_C_MACHINE) dev->category = unitype[i].category;
        if(unitype[i].device) dev->device = unitype[i].device;
        if(unitype[i].vendor) dev->vendor = unitype[i].vendor;
        if(unitype[i].model)  dev->model = unitype[i].model;
    }
}

/**
 * Look up a pci.ids record
 */
char *unitype_pciid(pciids_t **table, int numtable, uint32_t id)
{
    int s, e, h, i;

    if(table && numtable) {
        s = 0; e = numtable - 1;
        while(s <= e) {
            h = (e + s) >> 1;
            i = table[h]->id - id;
            if(!i) return (char*)&table[h]->name; else if(i > 0) e = h - 1; else s = h + 1;
        }
    }
    return NULL;
}

/**
 * Look up a pci.ids record by name
 */
uint32_t unitype_pciids_by_name(int model, char *str)
{
    pciids_t **table;
    int i, numtable;

    if(model) { table = models; numtable = nummodels; }
    else { table = vendors; numtable = numvendors; }

    if(str && *str) {
        if(numtable) {
            for(i = 0; i < numtable; i++)
                if(!strcmp(table[i]->name, str)) return table[i]->id;
        } else
        if(!nodb) { nodb = 1; msg("pci.ids not found, strings are not allowed in vendor and model fields\r\n"); }
    }
    return 0;
}

/**
 * Output device in human readable format
 */
char *unitype_out(gudt_device_t *dev)
{
    char *device = NULL, *vendor = NULL, *model = NULL;
    int o = 0;

    unitype_tmp[0] = 0;
    if(dev) {
        device = dev->category == GUDT_C_MACHINE ? (dev->device < 9 ? (char*)chassis[dev->device] : "Machine") :
            unitype_pciid(devices, numdevices, (dev->category << 16) | dev->device);
        vendor = unitype_pciid(vendors, numvendors, dev->vendor);
        model = unitype_pciid(models, nummodels, (dev->vendor << 16) | dev->model);
        o += snprintf(unitype_tmp + o, sizeof(unitype_tmp) - o, "%s", device ? device : "?");
        if(vendor) {
            o += snprintf(unitype_tmp + o, sizeof(unitype_tmp) - o, ", %s", vendor);
            if(model) o += snprintf(unitype_tmp + o, sizeof(unitype_tmp) - o, ", %s", model);
        }
    }
    return unitype_tmp;
}

/**
 * Dump the (potentially patched) Universal Device Type database to stdout
 */
void unitype_dump(void)
{
    int i;

    printf("# uses the database from https://pci-ids.ucw.cz/ as a baseline\r\n");
    printf("# maintained at https://gitlab.com/bztsrc/gudt/raw/main/gudt.ids\r\n\r\n");

    printf("# patch pci.ids database\r\n");
    printf("# V (vendorid) (name): patch/add vendor\r\n");
    for(i = 0; i < numvendors; i++)
        if(verbose || vendors[i]->patched)
            printf("V %04x %s\r\n", vendors[i]->id & 0xffff, vendors[i]->name);
    printf("# M (vendorid) (deviceid) (name): patch/add model\r\n");
    for(i = 0; i < nummodels; i++)
        if(verbose || models[i]->patched)
            printf("M %04x %04x %s\r\n", (models[i]->id >> 16) & 0xffff, models[i]->id & 0xffff, models[i]->name);
    printf("# C (class) (subclass) (name): patch/add category\r\n");
    for(i = 0; i < numdevices; i++)
        if(verbose || devices[i]->patched)
            printf("C %02x %02x %s\r\n", (devices[i]->id >> 16) & 0xff, devices[i]->id & 0xffff, devices[i]->name);

    printf("\r\n# (class) (sub-class) (vendorid) (deviceid) (driver/name)\r\n");
    for(i = 0; i < numunitype; i++)
        printf("%02x %02x %04x %04x %s\r\n", unitype[i].category, unitype[i].device, unitype[i].vendor, unitype[i].model,
            unitype[i].magic);
}

/**
 * Free resources
 */
void unitype_free(void)
{
    int i;

    if(unitype) {
        for(i = 0; i < numunitype; i++)
            if(unitype[i].magic) free(unitype[i].magic);
        free(unitype); unitype = NULL;
    }
    if(vendors) {
        for(i = 0; i < numvendors; i++)
            if(vendors[i]) free(vendors[i]);
        free(vendors); vendors = NULL;
    }
    if(models) {
        for(i = 0; i < nummodels; i++)
            if(models[i]) free(models[i]);
        free(models); models = NULL;
    }
    if(devices) {
        for(i = 0; i < numdevices; i++)
            if(devices[i]) free(devices[i]);
        free(devices); devices = NULL;
    }
    numunitype = numvendors = nummodels = numdevices = nodb = 0;
}

