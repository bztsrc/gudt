/*
 * src/main.h
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Command line Grand Unified Device Tree utility header file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "../gudt.h"

extern int snprintf (char *__s, size_t __maxlen, const char *__format, ...);
extern int vsnprintf (char *__s, size_t __maxlen, const char *__format, va_list args);

#define AML_MAXLEVEL 64
#define AML_MAXSTR   64
#define ARRAY_MAXITEMS 128

typedef union {
    char strs[65536];
    gudt_hdr_t hdr;
} gudt_hdr_u;

extern gudt_hdr_u gudt_hdr;
extern gudt_node_t *gudt_nodes;

typedef struct {
    uint32_t id;
    uint8_t patched;
    char name[28];
} __attribute__((packed)) pciids_t;
extern pciids_t **vendors, **models, **devices;
extern int numvendors, nummodels, numdevices;

extern const char *types[], *cats[], *chassis[], *units[], *levels[];

/* cli.c / gtk.c / win.c */
extern int verbose;
void msg(const char *fmt, ...);

/* file.c */
uint8_t* readfileall(char *file);
int writefileall(char *file, uint8_t *data, uint32_t size);

/* mem.c */
void mem_add(uint32_t type, uint64_t base, uint64_t size);
void mem_tonodes(void);
void mem_free(void);

/* unitype.c */
uint64_t gethex(uint8_t **s);
uint64_t getint(uint8_t **s);
uint32_t unitype_pciids_by_name(int model, char *str);
void unitype_init(void);
void unitype_node(gudt_device_t *dev);
char *unitype_out(gudt_device_t *dev);
void unitype_dump(void);
void unitype_free(void);

/* gudt.c */
void gudt_parse(uint8_t *ptr, char *fn);
void gudt_unpacked(char *fn);
void gudt_packed(char *fn);
void gudt_init(void);
uint32_t gudt_addstr(char *str);
uint32_t gudt_parent(char *name, char *driver);
void gudt_resource(char *name, char *driver, char *altdriver, uint32_t parent, int flags, int type, uint64_t arg1, uint64_t arg2);
void gudt_add(uint32_t name, uint32_t driver, uint32_t alt, uint32_t parent, int flags, int type, uint64_t arg1, uint64_t arg2);
void gudt_sort(void);
void gudt_free(void);

/* json.c */
void json_parse(uint8_t *ptr, char *fn);
void json_out(char *fn, int comments);
void tree_out(uint16_t idx, int lvl);

/* aml.c */
void aml_parse(uint8_t *ptr, char *fn);

/* fdt.c */
void fdt_parse(uint8_t *ptr, char *fn);

