/*
 * src/gudt.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Handle GUDT blobs
 */

#include <endian.h>
#if __BYTE_ORDER != __LITTLE_ENDIAN
#define GUDT_BIGENDIAN
#else
#define GUDT_NEEDCONV
#endif

#define GUDT_IMPLEMENTATION
#include "main.h"
#include "zlib.h"

gudt_hdr_u gudt_hdr;
gudt_node_t *gudt_nodes = NULL;
gudt_node_t *gudt_sorted = NULL;
uint32_t numsorted = 0, *sortidx;

uint32_t strs[32768];
int numstrs = 0;

/**
 * Parse a GUDT blob
 */
void gudt_parse(uint8_t *ptr, char *fn)
{
    gudt_hdr_t *hdr = (gudt_hdr_t*)ptr;
    gudt_device_t *dev, *par;
    gudt_node_t *nodes;
    uint32_t i, u, l;
    char *strs;

    if(memcmp(ptr, GUDT_MAGIC, 3) || (ptr[3] != 'T' && ptr[3] != 'B')) {
        msg("%s: not a valid GUDT\r\n", fn);
        return;
    }

    /* unpack compressed GUDT blobs. Use bitshifts instead of hdrsize because we might accidentally got a GUDB */
    if(ptr[3] == 'B') { i = ptr[5] | (ptr[4] << 8); l = ptr[7] | (ptr[6] << 8); }
    else { i = ptr[4] | (ptr[5] << 8); l = ptr[6] | (ptr[7] << 8); }
#ifdef GUDT_BIGENDIAN
    /* on big-endian machines gudt_unpack() does the swapping automatically */
    strs = (char*)malloc(i + 8 + l * sizeof(gudt_node_t));
    if(!strs) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
    if(!(i = gudt_unpack(hdr, (gudt_hdr_t *)strs)))
        { free(strs); msg("%s: unable to unpack\r\n", fn); return; }
    if(i == 2) { free(strs); strs = (char*)ptr; }
#else
    /* on little-endian machines */
    if(ptr[sizeof(gudt_hdr_t)] == 0x78 && ptr[sizeof(gudt_hdr_t) + 1] == 0xDA) {
        /* compressed blobs must always be little-endian */
        strs = (char*)malloc(i + 8 + l * sizeof(gudt_node_t));
        if(!strs) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
        if(!gudt_unpack(hdr, (gudt_hdr_t *)strs))
            { free(strs); msg("%s: unable to unpack\r\n", fn); return; }
    } else {
        /* as per the specification, uncompressed blobs should be little-endian too */
        strs = (char*)ptr;
        /* we should never receive a big-endian blob, but be safe than sorry */
        if(ptr[3] == 'B') gudt_bswap(hdr);
    }
#endif
    nodes = (gudt_node_t*)(strs + ((hdr->hdrsize + 7) & ~7));
    /* we can't just copy this into gudt_hdr + gudt_nodes, because if there are more inputs given then we must merge them */
    for(i = 0; i < hdr->numnodes; i++)
        if(nodes[i].parent < hdr->numnodes && nodes[nodes[i].parent].type == GUDT_T_DEVICE) {
            par = (gudt_device_t*)&nodes[nodes[i].parent];
            if(nodes[i].type == GUDT_T_DEVICE) {
                dev = (gudt_device_t*)&nodes[i];
                gudt_resource(dev->name >= 8 ? strs + dev->name : "", dev->driver >= 8 ? strs + dev->driver : "",
                    dev->alternative >= 8 ? strs + dev->alternative : "",
                    gudt_parent(par->name >= 8 ? strs + par->name : "", par->driver >= 8 ? strs + par->driver : ""),
                    dev->category, dev->type, dev->device, (dev->vendor << 16) | dev->model);
            } else
            if(nodes[i].type >= GUDT_T_NVSMEM && nodes[i].type <= GUDT_T_RAM) {
                mem_add(nodes[i].type, nodes[i].r.p.base, nodes[i].r.p.size);
            } else {
                u = GUDT_F_UNIT(nodes[i].flags);
                l = GUDT_F_DATA(nodes[i].flags);
                if(l)
                    gudt_resource(par->name >= 8 ? strs + par->name : "", par->driver >= 8 ? strs + par->driver : "", "", 0,
                        nodes[i].flags, nodes[i].type, u == 3 ? (uintptr_t)&nodes[i].r.p.base : (uintptr_t)&nodes[i].r, l);
                else
                    gudt_resource(par->name >= 8 ? strs + par->name : "", par->driver >= 8 ? strs + par->driver : "", "", 0,
                        nodes[i].flags, nodes[i].type, nodes[i].r.p.base, nodes[i].r.p.size);
            }
        }
    if(strs != (char*)ptr)
        free(strs);
}

/**
 * Output unpacked GUDT blob
 */
void gudt_unpacked(char *fn)
{
    uint8_t *data;
    uint32_t l = (gudt_hdr.hdr.hdrsize + 7) & ~7;
    uint32_t n = gudt_hdr.hdr.numnodes * sizeof(gudt_node_t);

    if(!gudt_nodes) return;
    data = (uint8_t*)malloc(l + n);
    if(!data) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
    memcpy(data, &gudt_hdr, l);
    memcpy(data + l, gudt_nodes, n);
    memcpy(data, GUDT_MAGIC, 4);
#ifdef GUDT_BIGENDIAN
    gudt_bswap((gudt_hdr_t*)data);
#endif
    writefileall(fn, data, l + n);
    free(data);
}

/**
 * Output packed GUDT blob
 */
void gudt_packed(char *fn)
{
    z_stream stream = { 0 };
    uint8_t *data;
#ifdef GUDT_BIGENDIAN
    uint8_t *src;
#endif
    uint32_t l = (gudt_hdr.hdr.hdrsize + 7) & ~7;
    uint32_t n = gudt_hdr.hdr.numnodes * sizeof(gudt_node_t);

    if(!gudt_nodes) return;
    stream.avail_out = compressBound(l + n) + sizeof(gudt_hdr_t);
    data = (uint8_t*)malloc(stream.avail_out);
    if(!data) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
    memcpy(data, &gudt_hdr, sizeof(gudt_hdr_t));
    memcpy(data, GUDT_MAGIC, 4);
    if(deflateInit(&stream, Z_BEST_COMPRESSION) != Z_OK) goto err;
    stream.next_out = data + sizeof(gudt_hdr_t);
    stream.next_in = (z_const Bytef *)gudt_hdr.strs + sizeof(gudt_hdr_t);
    stream.avail_in = l - sizeof(gudt_hdr_t);
    if(deflate(&stream, Z_NO_FLUSH) != Z_OK) goto err;
#ifdef GUDT_BIGENDIAN
    src = (uint8_t*)malloc(l + n);
    if(!src) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
    ((gudt_hdr_t*)src)->hdrsize = ((gudt_hdr_t*)data)->hdrsize = __builtin_bswap16(gudt_hdr.hdr.hdrsize);
    ((gudt_hdr_t*)src)->numnodes = ((gudt_hdr_t*)data)->numnodes = __builtin_bswap16(gudt_hdr.hdr.numnodes);
    memcpy(src + l, gudt_nodes, n);
    gudt_bswap((gudt_hdr_t*)src);
    stream.next_in = (z_const Bytef *)src + l;
#else
    stream.next_in = (z_const Bytef *)gudt_nodes;
#endif
    stream.avail_in = n;
    if(deflate(&stream, Z_FINISH) != Z_STREAM_END) goto err;
    deflateEnd(&stream);
    /* zlib might use other header bytes as well, make sure it's the right one */
    data[sizeof(gudt_hdr_t)] = 0x78; data[sizeof(gudt_hdr_t) + 1] = 0xDA;
    writefileall(fn, data, sizeof(gudt_hdr_t) + stream.total_out);
    free(data);
#ifdef GUDT_BIGENDIAN
    free(src);
#endif
    return;
err:msg("zlib compression error\r\n");
    free(data);
    return;
}

/**
 * Initialize in-memory structs
 */
void gudt_init(void)
{
    gudt_free();
    memset(&gudt_hdr, 0, sizeof(gudt_hdr));
    memset(&strs, 0, sizeof(strs));
    numstrs = 0;
}

/**
 * Add a string to string table, make sure it's unique
 */
uint32_t gudt_addstr(char *str)
{
    int i, l;

    if(!str || !*str) return 0;
    /* rtrim string (it is okay to modify the source string, actually it will fasten up subsequent calls) */
    for(i = strlen(str) - 1; i >= 0 && str[i] == ' '; i--) str[i] = 0;
    if(!*str) return 0;
    /* make sure the string is unique */
    for(i = 0; i < numstrs; i++)
        if(!strcmp(gudt_hdr.strs + strs[i], str)) return strs[i];
    l = strlen(str);
    /* check maximum buffer length */
    if(gudt_hdr.hdr.hdrsize < sizeof(gudt_hdr_t)) gudt_hdr.hdr.hdrsize = sizeof(gudt_hdr_t);
    if(numstrs == 32768 || (int)(uint32_t)gudt_hdr.hdr.hdrsize + l + 1 > 65535)
        return 0;
    /* add a zero byte if the first string happen to start with the zlib header (shouldn't, just a failsafe) */
    if(gudt_hdr.hdr.hdrsize == sizeof(gudt_hdr_t) && (*str == 0x78 || ((uint8_t*)str)[1] == 0xDA))
        gudt_hdr.hdr.hdrsize++;
    /* add string to string table */
    strs[numstrs++] = gudt_hdr.hdr.hdrsize;
    memcpy(gudt_hdr.strs + gudt_hdr.hdr.hdrsize, str, l);
    gudt_hdr.hdr.hdrsize += l + 1;
    return strs[i];
}

/**
 * Look up parent
 */
uint32_t gudt_parent(char *name, char *driver)
{
    gudt_device_t *devs = (gudt_device_t*)gudt_nodes;
    uint32_t i, n, d;
    uint16_t o;

    o = gudt_hdr.hdr.hdrsize;
    n = gudt_addstr(name);
    d = gudt_addstr(driver);
    for(i = 0; i < gudt_hdr.hdr.numnodes; i++)
        if(devs[i].type == GUDT_T_DEVICE && devs[i].name == n && devs[i].driver == d)
            return i;
    if(o < gudt_hdr.hdr.hdrsize) {
        memset(gudt_hdr.strs + o, 0, gudt_hdr.hdr.hdrsize - o);
        gudt_hdr.hdr.hdrsize = o;
    }
    return 0;
}

/**
 * Add a resource to list
 * @param name - device's unique name
 * @param driver - driver's name
 * @param altdriver - alternative driver's name (if any)
 * @param parent - parent node's index (only used for GUDT_T_DEVICE type nodes)
 * @param flags - bit 0..3: unit in shift bits, bit 4: inlined data
 * @param type - one of GUDT_T_x, or -1 for the root node
 * @param arg1 - base address, or when flags bit 4 set, pointer to the data to be inlined
 * @param arg2 - usually size, or number of items when flags bit 4 set
 */
void gudt_resource(char *name, char *driver, char *altdriver, uint32_t parent, int flags, int type, uint64_t arg1, uint64_t arg2)
{
    gudt_add(gudt_addstr(name), gudt_addstr(driver), gudt_addstr(altdriver), parent, flags, type, arg1, arg2);
}
void gudt_add(uint32_t name, uint32_t driver, uint32_t alt, uint32_t parent, int flags, int type, uint64_t arg1, uint64_t arg2)
{
    gudt_device_t *devs = (gudt_device_t*)gudt_nodes;
    uint32_t n, d, u, l;
    int i, par;

    if(name < 8 || name >= gudt_hdr.hdr.hdrsize || driver < 8 || driver >= gudt_hdr.hdr.hdrsize) return;
    if(alt < 8 || alt >= gudt_hdr.hdr.hdrsize) alt = 0;

    if(verbose > 1)
        msg("node flags %02x type %02x arg1 %08x%08x arg2 %08x%08x parent %u name '%s' driver '%s' alt '%s'\r\n",
            flags, type & 0xff, (uint32_t)(arg1 >> 32), (uint32_t)arg1, (uint32_t)(arg2 >> 32), (uint32_t)arg2, parent,
            name >= 8 ? gudt_hdr.strs + name : "", driver >= 8 ? gudt_hdr.strs + driver : "", alt >= 8 ? gudt_hdr.strs + alt : "");

    /* only add root node once */
    if(type == -1 || type == 0xff) {
        if(gudt_hdr.hdr.numnodes) return;
        type = GUDT_T_DEVICE; flags = GUDT_C_MACHINE; alt = parent = 0;
    }

    /* filter out typically bad records */
    if((type == GUDT_T_IOPORT && !arg1) || ((type == GUDT_T_DMA || type >= GUDT_T_MMIO) && !arg2))
        return;

    /* memory regions */
    if(type >= GUDT_T_NVSMEM && type <= GUDT_T_RAM) {
        if(!alt) { mem_add(type, arg1, arg2); return; }
        alt = 0;
    }

    /* let see if we have a device node for this resource */
    for(i = 0; i < (int)gudt_hdr.hdr.numnodes; i++)
        if(gudt_nodes[i].type == GUDT_T_DEVICE && devs[i].name == name && devs[i].driver == driver) {
            if(!devs[i].alternative) devs[i].alternative = alt;
            break;
        }
    par = i;

    /* add a new device node */
    if(i == gudt_hdr.hdr.numnodes) {
        if(!driver) return;
        /* fix buggy pci.ids */
        if(flags == 0x40) { flags = GUDT_C_PROCESSOR; arg1 = 0x40; }
        /* add node */
        gudt_hdr.hdr.numnodes++;
        gudt_nodes = (gudt_node_t*)realloc(gudt_nodes, gudt_hdr.hdr.numnodes * sizeof(gudt_node_t));
        if(!gudt_nodes) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
        devs = (gudt_device_t*)gudt_nodes;
        memset(&devs[i], 0, sizeof(gudt_device_t));
        devs[i].type = GUDT_T_DEVICE;
        devs[i].parent = parent;
        devs[i].category = flags;
        devs[i].name = name;
        devs[i].driver = driver;
        devs[i].alternative = alt;
        if(type == GUDT_T_DEVICE) {
            devs[i].device = flags ? arg1 : 0;
            devs[i].vendor = arg2 >> 16;
            devs[i].model = arg2 >> 16 ? arg2 & 0xffff : 0;
        }
        unitype_node(&devs[i]);
    }
    if(type == GUDT_T_DEVICE) return;
    /* IRQs are special, because without an affinity level they might be converted into an inlined list */
    if(type == GUDT_T_IRQ && arg1 < 256 && !arg2) {
        for(i = 0; i < (int)gudt_hdr.hdr.numnodes && (gudt_nodes[i].type != type || gudt_nodes[i].parent != par ||
            (!GUDT_F_DATA(gudt_nodes[i].flags) && gudt_nodes[i].r.p.size) || GUDT_F_DATA(gudt_nodes[i].flags) == 12); i++);
        if(i == gudt_hdr.hdr.numnodes) goto add;
        l = GUDT_F_DATA(gudt_nodes[i].flags);
        if(!l) {
            gudt_nodes[i].flags = 0x10; l = 1;
            gudt_nodes[i].r.b.data[0] = gudt_nodes[i].r.p.base;
        }
        for(d = 0; d < l; d++)
            if(gudt_nodes[i].r.b.data[d] == arg1) return;
        gudt_nodes[i].r.b.data[d] = arg1;
        gudt_nodes[i].flags += 0x10;
        return;
    }
    /* do we already have this resource? */
    for(i = 0; i < (int)gudt_hdr.hdr.numnodes && (gudt_nodes[i].type != type || gudt_nodes[i].parent != par ||
        GUDT_F_DATA(gudt_nodes[i].flags) || gudt_nodes[i].r.p.base != arg1); i++);
    /* add a new resource node */
    if(i == gudt_hdr.hdr.numnodes) {
add:    if(GUDT_F_DATA(flags)) {
            u = GUDT_F_UNIT(flags);
            l = u == 1 ? 12 : (u == 2 ? 6 : (u == 4 ? 3 : 1));
            if(u > 8 || !arg1 || !arg2) {
                msg("resource '%s' bad inlined data (u %u)\r\n", gudt_hdr.strs + name, u);
                return;
            }
            n = (arg2 + l - 1) / l;
        } else {
            l = u = 0;
            n = 1;
        }
        /* for inlined data, we might need to allocate multiple nodes */
        for(d = 0; d < n; d++) {
            gudt_hdr.hdr.numnodes++;
            gudt_nodes = (gudt_node_t*)realloc(gudt_nodes, gudt_hdr.hdr.numnodes * sizeof(gudt_node_t));
            if(!gudt_nodes) { fprintf(stderr, "gudt: unable to allocate memory\r\n"); exit(1); }
            memset(&gudt_nodes[i], 0, sizeof(gudt_node_t));
            gudt_nodes[i].type = type;
            gudt_nodes[i].parent = par;
            if(l) {
                if(arg2 < l) l = arg2;
                gudt_nodes[i].flags = (l << 4) | (flags & 0xf);
                if(u == 8) memcpy(&gudt_nodes[i].r.q.data, (void*)(uintptr_t)arg1, u);
                else       memcpy(&gudt_nodes[i].r.b.data, (void*)(uintptr_t)arg1, l * u);
                arg1 += l * u;
                arg2 -= l;
            } else {
                gudt_nodes[i].flags = flags & 0xf;
                gudt_nodes[i].r.p.size = arg2;
                gudt_nodes[i].r.p.base = arg1;
            }
        }
    } else
    if(gudt_nodes[i].type == type && gudt_nodes[i].parent == par && !GUDT_F_DATA(gudt_nodes[i].flags) &&
      gudt_nodes[i].r.p.base == arg1 && gudt_nodes[i].r.p.size < arg2) gudt_nodes[i].r.p.size = arg2;
}

/**
 * Add nodes to the sorted array
 */
void gudt_sortadd(uint32_t idx, int lvl)
{
    uint32_t i, j;

    if(idx >= gudt_hdr.hdr.numnodes || lvl > 64) return;

    /* copy the parent node */
    sortidx[idx] = numsorted;
    memcpy(&gudt_sorted[numsorted++], &gudt_nodes[idx], sizeof(gudt_node_t));

    /* recursively parse children resource nodes */
    for(j = 1; j < 255; j++)
        for(i = 0; i < gudt_hdr.hdr.numnodes; i++)
            if(i != idx && gudt_nodes[i].type == j && gudt_nodes[i].parent == idx)
                gudt_sortadd(i, lvl + 1);

    /* recursively parse children device nodes */
    for(i = 0; i < gudt_hdr.hdr.numnodes; i++)
        if(i != idx && gudt_nodes[i].type == GUDT_T_DEVICE && gudt_nodes[i].parent == idx)
            gudt_sortadd(i, lvl + 1);
}

/**
 * Sort nodes so that parents always preceed their children nodes
 */
void gudt_sort(void)
{
    uint32_t i;

    gudt_sorted = (gudt_node_t*)malloc(gudt_hdr.hdr.numnodes * sizeof(gudt_node_t));
    if(!gudt_sorted) return;
    if((sortidx = (uint32_t*)malloc(gudt_hdr.hdr.numnodes * sizeof(uint32_t)))) {
        /* find the root node (should be the first, but be bullet-proof) */
        for(i = 0; i < gudt_hdr.hdr.numnodes &&
            (gudt_nodes[i].type != GUDT_T_DEVICE || ((gudt_device_t*)&gudt_nodes[i])->category != GUDT_C_MACHINE); i++);
        /* copy nodes sorted and replace gudt_nodes list */
        if(i < gudt_hdr.hdr.numnodes) {
            numsorted = 0;
            gudt_sortadd(i, 0);
            /* patch node indeces with the new, sorted index */
            for(i = 0; i < gudt_hdr.hdr.numnodes; i++) {
                gudt_sorted[i].parent = sortidx[gudt_sorted[i].parent];
                if(gudt_sorted[i].type == GUDT_T_INTC || gudt_sorted[i].flags == GUDT_F_INDIRECT)
                    gudt_sorted[i].r.p.size = sortidx[gudt_sorted[i].r.p.size];
            }
            /* replace nodes list with the sorted list */
            free(gudt_nodes);
            gudt_nodes = gudt_sorted;
        } else
            free(gudt_sorted);
        free(sortidx);
    }
}

/**
 * Free all resources
 */
void gudt_free(void)
{
    if(gudt_nodes) { free(gudt_nodes); gudt_nodes = NULL; }
    mem_free();
    memset(&gudt_hdr, 0, sizeof(gudt_hdr));
    numstrs = 0;
}
