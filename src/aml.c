/*
 * src/aml.c
 * https://gitlab.com/bztsrc/gudt
 *
 * Copyright (C) 2024 bzt, GPLv3+
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief ACPI AML bytecode parser
 */

#include "main.h"

typedef struct {
    uint8_t magic[4];
    uint32_t size;
    uint8_t rev;
    uint8_t chksum;
    char OEM[6];
    char OEMtableid[8];
    uint32_t OEMrev;
    uint32_t creatid;
    uint32_t creatrev;
} __attribute__((packed)) acpi_t;

typedef struct {
    uint8_t     space;
    uint8_t     width;
    uint8_t     offset;
    uint8_t     type;
    uint64_t    address;
} __attribute__((packed)) gas_t;

typedef struct {
    uint8_t type;   /* 0 processor */
    uint8_t size;   /* 8 */
    uint8_t acpi_id;
    uint8_t apic_id;
    uint32_t flags; /* bit 0: enabled, bit 1: available */
} __attribute__((packed)) madt_cpu_t;

typedef struct {
    uint8_t type;   /* 1 IO apic */
    uint8_t size;   /* 12 */
    uint16_t ioapic_id;
    uint32_t address;
    uint32_t int_base;
} __attribute__((packed)) madt_ioapic_t;

typedef struct {
    uint8_t type;   /* 5 local apic */
    uint8_t size;   /* 12 */
    uint16_t reserved;
    uint64_t address;
} __attribute__((packed)) madt_lapic_t;

typedef struct {
    uint8_t type;   /* 9 processor */
    uint8_t size;   /* 16 */
    uint16_t reserved;
    uint32_t apic_id;
    uint32_t flags; /* bit 0: enabled, bit 1: available */
    uint32_t acpi_id;
} __attribute__((packed)) madt_cpu2_t;

typedef struct {
    acpi_t hdr;     /* magic "APIC" */
    uint32_t lapic_addr;
    uint32_t flags;
} __attribute__((packed)) madt_t;

typedef struct {
    acpi_t hdr;     /* magic "FACP" */
    uint32_t firmware_ctrl;
    uint32_t dsdt;
    uint8_t reserved;
    uint8_t preferred_pm_profile;
    uint16_t sci_int;
    uint32_t smi_cmd;
    uint8_t acpi_enable;
    uint8_t acpi_disable;
    uint8_t s4bios_req;
    uint8_t pstate_cnt;
    uint32_t pm1a_evt_blk;
    uint32_t pm1b_evt_blk;
    uint32_t pm1a_cnt_blk;
    uint32_t pm1b_cnt_blk;
    uint32_t pm2_cnt_blk;
    uint32_t pm_tmr_blk;
    uint32_t gpe0_blk;
    uint32_t gpe1_blk;
    uint8_t pm1_evt_len;
    uint8_t pm1_cnt_len;
    uint8_t pm2_cnt_len;
    uint8_t pm_tmr_len;
    uint8_t gpe0_blk_len;
    uint8_t gpe1_blk_len;
    uint8_t gpe1_base;
    uint8_t cst_cnt;
    uint16_t p_lvl2_lat;
    uint16_t p_lvl3_lat;
    uint16_t flush_size;
    uint16_t flush_stride;
    uint8_t duty_offset;
    uint8_t duty_width;
    uint8_t day_alarm;
    uint8_t mon_alarm;
    uint8_t century;
    uint16_t iapc_boot_arch;
    uint8_t reserved2;
    uint32_t flags;
    gas_t reset_reg;
    uint8_t reset_value;
    uint8_t reserved3[3];
    uint64_t x_firmware_control;
    uint64_t x_dsdt;
    gas_t x_pm1a_evt_blk;
    gas_t x_pm1b_evt_blk;
    gas_t x_pm1a_cnt_blk;
    gas_t x_pm1b_cnt_blk;
    gas_t x_pm2_cnt_blk;
    gas_t x_pm_tmr_blk;
    gas_t x_gpe0_blk;
    gas_t x_gpe1_blk;
    gas_t sleep_control_reg;
    gas_t sleep_status_reg;
} __attribute__((packed)) fadt_t;

typedef struct {
    uint32_t type;
    uint32_t parent;
    uint8_t *name;
    union {
        uint8_t  b[16];
        uint16_t w[8];
        uint32_t d[4];
        uint64_t q[2];
        uint8_t *p[2];
    } data;
} aml_t;

static struct {
    aml_t *node;
    uint64_t stack[AML_MAXLEVEL*16];
    uint32_t len, num, max, par, sp;
    uint32_t mtdidx, devidx, drvidx, altidx, regidx;
    uint8_t *memmin, *memmax;
} aml;
#define NODE aml.node[aml.num]

static uint8_t *aml_NameString(uint8_t *ptr);
static uint8_t *aml_TermList(uint8_t *ptr, uint8_t *end, aml_t *node);

#ifdef DEBUG
static uint8_t *base = NULL;
static uint32_t numchk, stkchk, stkmax, lvl;
static char *space(void) {uint32_t i;fprintf(stderr,"%2u ", lvl);for(i=0;i<lvl;i++){fprintf(stderr," ");}return "";}
#define AML_STKENTER(s) do{ stkchk += s; if(stkchk > stkmax) stkmax = stkchk; }while(0)
#define AML_STKLEAVE(s) stkchk -= s

/**
 * Dump AML nodes
 */
void aml_dump(aml_t *node)
{
    uint32_t i, j, n;
    if(node) n = 1; else { node = &aml.node[0]; n = aml.num; }
    for(i = 0; i < n; i++, node++) {
        fprintf(stderr, "%4d parent %4d type %04x name '%c%c%c%c' data.q ", i, node->parent,node->type,
            node->name && node->name[0] > 32 && node->name[0] < 127 ? node->name[0] : '.',
            node->name && node->name[1] > 32 && node->name[1] < 127 ? node->name[1] : '.',
            node->name && node->name[2] > 32 && node->name[2] < 127 ? node->name[2] : '.',
            node->name && node->name[3] > 32 && node->name[3] < 127 ? node->name[3] : '.');
        for(j = 0; j < 2; j++) fprintf(stderr, " %lx", node->data.q[j]);
        fprintf(stderr, " data.d ");
        for(j = 0; j < 4; j++) fprintf(stderr, " %x", node->data.d[j]);
        fprintf(stderr, "\r\n");
    }
}
#else
#define AML_STKENTER(s)
#define AML_STKLEAVE(s)
#endif

/**
 * Lookup AML node
 */
static uint32_t aml_lookup(uint8_t *name)
{
    uint32_t parent = aml.par, i, n, c, a = 2;
    uint8_t *s;

#ifdef DEBUG
    if(stkchk + 32 > stkmax) stkmax = stkchk + 32;
#endif
    if(name < aml.memmin || name >= aml.memmax) return 0;
    if(*name == '\\') { name++; parent = 0; }
    while(*name == '^' && parent) { name++; parent = aml.node[parent].parent; }
    switch(*name) {
        case 0x00: return 0;
        case 0x2E: name++; c = 2; break;
        case 0x2F: name++; c = *name++; break;
        default: c = 1; break;
    }
    i = aml.num - 1;
    if(c == 1 && (aml.node[i].parent == parent || aml.node[i].parent == aml.node[parent].parent) &&
      !memcmp(aml.node[i].name, name, 4)) return i;
    do {
        s = name; n = c;
again:  for(i = 1; i < aml.num; i++) {
            if(aml.node[i].parent == parent && !memcmp(aml.node[i].name, s, 4)) {
                n--; s += 4; parent = i;
                if(!n) return i; else goto again;
            }
        }
        parent = a == 2 ? aml.node[aml.par].parent : 0;
    } while(a--);
    if(c == 1)
        for(i = aml.num - 1; i > 0; i--)
            if(!memcmp(aml.node[i].name, name, 4)) return i;
#ifdef DEBUG
    if(verbose) {
        fprintf(stderr, "gudt: node not found, parent %d '", aml.par);
        for(n = 0; n < c; n++)
            for(i = 0; i < 4; i++) {
                if(n) printf(".");
                if(name[i] > 32 && name[i] < 127) fprintf(stderr, "%c", name[i]); else fprintf(stderr, "\\x%02X", name[i]);
            }
        fprintf(stderr, "'\r\n");
    }
#endif
    return 0;
}

/**
 * Read AML opcode
 */
static uint8_t *aml_opcode(uint8_t *ptr, uint16_t *op)
{
#ifdef DEBUG
    if(stkchk + 16 > stkmax) stkmax = stkchk + 16;
#endif
    if(ptr < aml.memmin || ptr >= aml.memmax) { *op = 0xffff; return ptr; }
    if(*ptr == '\\' || *ptr == '^' || *ptr == '/' || *ptr == '_' || *ptr == '.' || (*ptr >= 'A' && *ptr <= 'Z')) {
        *op = 9; ptr = aml_NameString(ptr);
    } else
    if(*ptr == 0x5B || (*ptr == 0x92 && ptr[1] >= 0x93 && ptr[1] <= 0x95)) { *op = *ptr++ << 8; *op |= *ptr++; }
    else *op = *ptr++;

    return ptr;
}

/**
 * AML packet
 */
static uint8_t *aml_pkg(uint8_t *ptr)
{
    uint8_t imm = (*ptr & 0xC0) >> 6, i;
    aml.len = imm ? (*ptr++ & 0x0F) : (*ptr++ & 0x3F);
    for(i = 0; i < imm; i++) aml.len |= *ptr++ << (4 + (i << 3));
    return ptr;
}

/**
 * Count number of nodes
 */
static int aml_numnodes(int siz, acpi_t **tbl)
{
    int i, n = 0, cur, l = 0, nodes[AML_MAXLEVEL] = { 0 }, skip = 0, skipbytes = 0, skip2 = 0, skip2bytes = 0;
    uint8_t *st, *ptr, *end, *scope[AML_MAXLEVEL] = { 0 };
    uint16_t op;

    cur = 1;
    memset(&aml, 0, sizeof(aml)); aml.memmin = (uint8_t*)-1UL;
    for(i = 0; i < siz && tbl; i++) {
        if(!tbl[i] || !tbl[i]->size) continue;
        if(tbl[i]->magic[0] == 0xD0 && tbl[i]->magic[1] == 0x0D) { n++; continue; }
        ptr = (uint8_t*)tbl[i] + sizeof(acpi_t); end = ptr + tbl[i]->size;
        if(ptr[0] == 0xD0 && ptr[1] == 0x0D) { n++; continue; }
        if(aml.memmin > ptr) aml.memmin = ptr;
        if(aml.memmax < end) aml.memmax = end;
        while(ptr < end) {
            if(cur > n) n = cur;
            while(l > 0 && ptr >= scope[l - 1]) cur = nodes[--l];
            if(skip) { skip--; if(!skip) { ptr += skipbytes; skipbytes = 0; } }
            if(skip2) { skip2--; if(!skip2) { ptr += skip2bytes; skip2bytes = 0; } }
            st = ptr; ptr = aml_opcode(ptr, &op);
            switch(op) {
                case 0x0006: case 0x0008: ptr = aml_NameString(ptr); cur++; break;
                case 0x0009: break;
                case 0x000A: ptr++; break;
                case 0x000B: ptr += 2; break;
                case 0x000C: ptr += 4; break;
                case 0x000E: ptr += 8; break;
                case 0x000D: for(; ptr < end && ptr[-1]; ptr++); break;
                case 0x0011: case 0x0012: case 0x0013: case 0x5B83:
                    ptr = aml_pkg(ptr); ptr = st + (op > 0xff ? 2 : 1) + aml.len; break;
                case 0x5B87: case 0x5B88:
                    ptr = aml_pkg(ptr); ptr = st + (op > 0xff ? 2 : 1) + aml.len; cur++; break;
                case 0x0010: case 0x5B82:
                    ptr = aml_pkg(ptr); cur++; if(l < AML_MAXLEVEL) { scope[l] = st + 1 + aml.len; nodes[l++] = cur; }
                break;
                case 0x0014:
                    ptr = aml_pkg(ptr); cur++; st += 1 + aml.len;
                    if(l < AML_MAXLEVEL && (!memcmp(ptr, "_HID", 4) || !memcmp(ptr, "_CID", 4) || !memcmp(ptr, "_CRS", 4))) {
                        scope[l] = st; nodes[l++] = cur;
                    } else ptr = st;
                break;
                case 0x0015: ptr = aml_NameString(ptr) + 2; break;
                case 0x00A0: case 0x00A1: case 0x00A2: ptr = aml_pkg(ptr); break;
                case 0x5B01: ptr = aml_NameString(ptr) + 1; break;
                case 0x5B02: ptr = aml_NameString(ptr); break;
                case 0x5B23: skip = 1; skipbytes = 2; break;
                case 0x5B80: ptr = aml_NameString(ptr) + 1; cur++; break;
                case 0x5B81: case 0x5B86: ptr = aml_pkg(ptr); st += 2 + aml.len;
                    ptr = aml_NameString(ptr);
                    if(op == 0x5B86) ptr = aml_NameString(ptr);
                    ptr++;
                    while(ptr < st) {
                        switch(*ptr) {
                            case 0: ptr++; ptr = aml_pkg(ptr); break;
                            case 1: ptr += 3; break;
                            case 2: ptr = aml_NameString(ptr + 1); break;
                            case 3: ptr += 3 + ptr[2]; break;
                            default: ptr += 4; ptr = aml_pkg(ptr); cur++; break;
                        }
                    }
                    ptr = st; cur++;
                break;
                case 0x0089: skip = 1; skipbytes = 1; skip2 = 2; skip2bytes = 1; break;
                case 0x008A: case 0x008B: case 0x008C: case 0x008D: case 0x008F: case 0x5B13: cur++; break;
                default: break;
            }
        }
    }
    n += AML_MAXLEVEL*4;
#ifdef AML_MINNODES
    if(n < AML_MINNODES) n = AML_MINNODES;
#endif
    aml.num = 1; aml.max = n; aml.sp = AML_MAXLEVEL*16;
    aml.mtdidx = aml.devidx = aml.drvidx = aml.regidx = -1U;
#ifdef DEBUG
    numchk = lvl = 0; stkchk = stkmax = n * sizeof(aml_t) + 16;
#endif
    return n;
}

/**
 * Filename part of path
 */
static void aml_fn(aml_t *node)
{
    int c;

#ifdef DEBUG
    if(stkchk + 24 > stkmax) stkmax = stkchk + 24;
#endif
    if(node->name[0] == '\\') { node->name++; node->parent = 0; }
    while(node->name[0] == '^' && node->parent) { node->name++; node->parent = aml.node[node->parent].parent; }
    if(node->name[0] == 0x2E) node->name += 4; else
    if(node->name[0] == 0x2F) { node->name++; c = (node->name[0] - 1) << 2; node->name += 1 + c; }
}

/**
 * Load AML node value
 */
static void aml_get(aml_t *val, aml_t *in)
{
    uint32_t idx;

#ifdef DEBUG
    if(stkchk + 24 > stkmax) stkmax = stkchk + 24;
#endif
    if(in->type >= 0x0060 && in->type <= 0x006F) {
        idx = aml.sp + in->type - 0x60;
        val->data.q[0] = idx < AML_MAXLEVEL*16 ? aml.stack[idx] : 0;
        val->type = 0x000E;
    } else
    if(in->type == 0x5B13) {
        val->data.q[0] = 0;
        val->type = 0x000E;
        if(in->data.p[0] >= aml.memmin && in->data.p[0] < aml.memmax) {
            if(!(in->data.d[2] & 7) && !(in->data.d[3] & 7))
                memcpy(&val->data, (void*)(in->data.q[0] + (in->data.d[2] >> 3)),
                    (in->data.d[3] >= 64 ? 64 : in->data.d[3]) >> 3);
            else {
                val->data.q[0] = *((uint64_t*)(in->data.q[0] + (in->data.d[2] >> 3))) >> (in->data.d[2] & 7);
            }
        }
    } else
    if(in->type == 0x0011 || in->type == 0x0012 || in->type == 0x0013) {
        val->data.q[0] = 0;
        val->type = 0x000E;
        if(in->data.p[0] >= aml.memmin && in->data.p[0] < aml.memmax)
            memcpy(&val->data, in->data.p[0], in->data.d[2] >= 8 ? 8 : in->data.d[2]);
    } else
    if(val != in) memcpy(val, in, sizeof(aml_t));
}

/**
 * Set AML node value
 */
static void aml_set(aml_t *out, aml_t *val)
{
    uint64_t num;

#ifdef DEBUG
    if(stkchk + 24 > stkmax) stkmax = stkchk + 24;
#endif
    if(out->type == 0x000D && !out->data.p[0]) return;
    if(out->type >= 0x0060 && out->type <= 0x006F) {
        num = aml.sp + out->type - 0x60;
        if(num < AML_MAXLEVEL*16) aml.stack[num] = val->data.q[0];
        out->data.q[0] = val->data.q[0];
    } else
    if(out->type == 0x5B13) {
        if(out->data.p[0] >= aml.memmin && out->data.p[0] < aml.memmax) {
            if(!(out->data.d[2] & 7) && !(out->data.d[3] & 7))
                memcpy((void*)(out->data.q[0] + (out->data.d[2] >> 3)), &val->data, out->data.d[3] >> 3);
            else {
                num = *((uint64_t*)(out->data.q[0] + (out->data.d[2] >> 3)));
                num &= ~(~(-1UL << (out->data.d[3])) << (out->data.d[2] & 7));
                num |= (val->data.q[0] & ~(-1UL << (out->data.d[3]))) << (out->data.d[2] & 7);
                *((uint64_t*)(out->data.q[0] + (out->data.d[2] >> 3))) = num;
            }
        }
    } else
    if(out->type == 0x0011 || out->type == 0x0012 || out->type == 0x0013) {
        if(out->data.p[0] >= aml.memmin && out->data.p[0] < aml.memmax)
            memcpy(out->data.p[0], &val->data, out->data.d[2] >= 8 ? 8 : out->data.d[2]);
    } else
    if(out->type == val->type || (out->type ==0x000E && val->type >= 0x60 && val->type <= 0x006F))
        memcpy(&out->data, &val->data, sizeof(out->data));
#ifdef DEBUG
    else if(out->type != 0x0009 && verbose > 2) { fprintf(stderr, "Bad lvalue\r\n"); aml_dump(out); }
#endif
}

/**
 * AML code to string
 */
static void aml_drvname(uint32_t idx, char *str)
{
    int i;
    if(idx < 1 || idx >= aml.num) return;
    if(aml.node[idx].type == 0x000C) {
        str[0] = '@' + ((aml.node[idx].data.d[0] >> 2) & 0x1F);
        str[1] = '@' + ((aml.node[idx].data.d[0] << 3) & 0x18) + ((aml.node[idx].data.d[0] >> 13) & 0x7);
        str[2] = '@' + ((aml.node[idx].data.d[0] >> 8) & 0x1F);
        i = (aml.node[idx].data.d[0] >> 20) & 0xF; str[3] = i < 10 ? '0' + i : 'A' + i - 10;
        i = (aml.node[idx].data.d[0] >> 16) & 0xF; str[4] = i < 10 ? '0' + i : 'A' + i - 10;
        i = (aml.node[idx].data.d[0] >> 28) & 0xF; str[5] = i < 10 ? '0' + i : 'A' + i - 10;
        i = (aml.node[idx].data.d[0] >> 24) & 0xF; str[6] = i < 10 ? '0' + i : 'A' + i - 10;
    } else
    if(aml.node[idx].type == 0x000D && aml.node[idx].data.p[0] >= aml.memmin &&
      aml.node[idx].data.p[0] < aml.memmax) {
        for(i = 0; i < 30 && aml.node[idx].data.p[0][i]; i++)
            str[i] = (char)aml.node[idx].data.p[0][i];
    }
}

/**
 * Parse resource template
 */
static void aml_ResourceTemplate(void)
{
    uint8_t *ptr, *end, t;
    uint16_t s, n, rg = 0;
    uint32_t par = 0;
    uint64_t rs, re, ra, rt, rl;
    char name[8], drv[AML_MAXSTR], alt[AML_MAXSTR];
    int i;

    memset(name, 0, sizeof(name)); memset(drv, 0, sizeof(drv));
    if(aml.node[aml.par].name >= aml.memmin && aml.node[aml.par].name < aml.memmax) {
        memcpy(name, aml.node[aml.par].name, 4);
        aml_drvname(aml.par, drv);
        par = gudt_parent(name, drv);
    }

    memset(name, 0, sizeof(name)); memset(drv, 0, sizeof(drv)); memset(alt, 0, sizeof(alt));
    if(aml.node[aml.devidx].name >= aml.memmin && aml.node[aml.devidx].name < aml.memmax)
        memcpy(name, aml.node[aml.devidx].name, 4);
    aml_drvname(aml.drvidx, drv); aml_drvname(aml.altidx, alt);
    if(!memcmp(drv, alt, sizeof(alt))) memset(alt, 0, sizeof(alt));
#ifdef DEBUG
    if(verbose > 1) {
        fprintf(stderr, "-----------------add resource dev %d '%s' drv %d(%d) '%s'('%s') reg %d-------------\r\n",
            aml.devidx, name, aml.drvidx, aml.altidx, drv, alt, aml.regidx);
        aml_dump(&aml.node[aml.devidx]);
        aml_dump(&aml.node[aml.drvidx]);
        if(aml.altidx != -1U) aml_dump(&aml.node[aml.altidx]);
        aml_dump(&aml.node[aml.regidx]);
        fprintf(stderr, "--------------------------------------------------------------\r\n");
    }
#endif
    if(drv[0]) {
        ptr = aml.node[aml.regidx].data.p[0]; end = aml.node[aml.regidx].data.p[0] + aml.node[aml.regidx].data.d[2];
        if(aml.node[aml.regidx].type == 0x0011 && ptr >= aml.memmin && end < aml.memmax) {
            /* ResourceTemplate, see ACPI Specification section 6.4 Resource Data Types for ACPI */
#ifdef DEBUG
            if(verbose > 2) fprintf(stderr, "%sResourceTemplate %08lx %08lx\r\n", space(), ptr - base, end - base);
#endif
            while(ptr < end) {
                if(*ptr & 0x80) { t = *ptr; s = (ptr[2] << 8) | ptr[1]; ptr += 3; }
                else { t = *ptr >> 3; s = *ptr & 7; ptr++; }
#ifdef DEBUG
                if(verbose > 2) {
                    fprintf(stderr, "%st %02x s %d ", space(), t, s);
                    for(i = 0; i < (int)s; i++) { fprintf(stderr, " %02x", ptr[i]); }
                    fprintf(stderr, "\r\n");
                }
#endif
                switch(t) {
                    case 0x04: /* IRQ Format Descriptor */
                        for(n = ptr[0] | (ptr[1] << 8), i = 0; i < 16; i++)
                            if(n & (1 << i)) gudt_resource(name, drv, alt, par, 0, GUDT_T_IRQ, i, 0);
                    break;
                    case 0x05: /* DMA Format Descriptor */
                        for(n = ptr[0], i = 0; i < 8; i++)
                            if(n & (1 << i)) gudt_resource(name, drv, alt, par, ptr[1] & 3 ? 1 : 0, GUDT_T_DMA, i, 1);
                    break;
                    case 0x08: /* I/O Port Descriptor */
                        for(rs = (uint64_t)ptr[1] | ((uint64_t)ptr[2] << 8), re = (uint64_t)ptr[3] | ((uint64_t)ptr[4] << 8);
                          rs <= re; rs += ptr[5] ? ptr[5] : 1)
                            if(rs) gudt_resource(name, drv, alt, par, 0, GUDT_T_IOPORT, rs, ptr[6] ? ptr[6] : (ptr[5] ? ptr[5] : 1));
                    break;
                    case 0x09: /* Fixed Location I/O Port Descriptor */
                        ra = ptr[0] | (ptr[1] << 8);
                        if(ra) gudt_resource(name, drv, alt, par, 1, GUDT_T_IOPORT, ra, ptr[2] ? ptr[2] : 1);
                    break;
                    case 0x0A: /* Fixed DMA Descriptor */
                        gudt_resource(name, drv, alt, par, ptr[4] & 0x0f, GUDT_T_DMA, ptr[0] | (ptr[1] << 8), 1 << ptr[4]);
                    break;
                    case 0x81: /* 24-Bit Memory Range Descriptor */
                        for(rs = (uint64_t)ptr[1] | ((uint64_t)ptr[2] << 8), re = (uint64_t)ptr[3] | ((uint64_t)ptr[4] << 8),
                            ra = ((uint64_t)ptr[5] << 16) | ((uint64_t)ptr[6] << 24); rs <= re; rs += ra) {
                            if(rs) { gudt_resource(name, drv, alt, par, 0, GUDT_T_MMIO, rs, ((uint64_t)ptr[7] << 8) | ((uint64_t)ptr[8] << 16)); }
                            if(!ra) ra = 65536;
                        }
                    break;
                    case 0x82: /* Generic Register Descriptor */
                        memcpy(&ra, ptr + 4, 8); rs = ptr[5] ? 1 << (ptr[5]-1) : 1;
                        if(ptr[0] < 32) gudt_resource(name, drv, alt, par, 0, ptr[0] + GUDT_T_MMIO, ra, rs);
                    break;
                    case 0x85: /* 32-Bit Memory Range Descriptor */
                        for(rs = (uint64_t)ptr[1] | ((uint64_t)ptr[2] << 8) | ((uint64_t)ptr[3] << 16) | ((uint64_t)ptr[4] << 24),
                            re = (uint64_t)ptr[5] | ((uint64_t)ptr[6] << 8) | ((uint64_t)ptr[7] << 16) | ((uint64_t)ptr[8] << 24),
                            ra = (uint64_t)ptr[9] | ((uint64_t)ptr[10] << 8) | ((uint64_t)ptr[11] << 16) | ((uint64_t)ptr[12] << 24);
                            rs <= re; rs += ra) {
                            if(rs) { gudt_resource(name, drv, alt, par, 0, GUDT_T_MMIO, rs, ptr[13] | (ptr[14] << 8) | (ptr[15] << 16) | (ptr[16] << 24)); }
                            if(!ra) ra = 65536;
                        }
                    break;
                    case 0x86: /* 32-Bit Fixed Memory Range Descriptor */
                        rs = (uint64_t)ptr[1] | ((uint64_t)ptr[2] << 8) | ((uint64_t)ptr[3] << 16) | ((uint64_t)ptr[4] << 24);
                        re = (uint64_t)ptr[5] | ((uint64_t)ptr[6] << 8) | ((uint64_t)ptr[7] << 16) | ((uint64_t)ptr[8] << 24);
                        if(rs) { gudt_resource(name, drv, alt, par, 0, GUDT_T_MMIO, rs, re); }
                    break;
                    case 0x87: /* Address Space Resource Descriptors */
                        ra = ((uint64_t)ptr[3] | ((uint64_t)ptr[4] << 8) | ((uint64_t)ptr[5] << 16) | ((uint64_t)ptr[6] << 24)) + 1;
                        rs = ((uint64_t)ptr[7] | ((uint64_t)ptr[8] << 8) | ((uint64_t)ptr[9] << 16) | ((uint64_t)ptr[10] << 24));
                        re = ((uint64_t)ptr[11] | ((uint64_t)ptr[12] << 8) | ((uint64_t)ptr[13] << 16) | ((uint64_t)ptr[14] << 24));
                        rt = ((uint64_t)ptr[15] | ((uint64_t)ptr[16] << 8) | ((uint64_t)ptr[17] << 16) | ((uint64_t)ptr[18] << 24));
                        rl = ((uint64_t)ptr[19] | ((uint64_t)ptr[20] << 8) | ((uint64_t)ptr[21] << 16) | ((uint64_t)ptr[22] << 24));
                        rg = 2;
asd:                    if(ptr[1] & 2) rt = -rt;
                        if(ra < rl) ra = rl;
                        if(!ra) ra = !ptr[0] ? 65536 : 1;
                        t = ((ptr[2] >> 3) & 3) == 3 ? GUDT_T_NVSMEM : GUDT_T_MMIO;
                        for(; rs <= re; rs += ra) {
                            if(rs)
                                switch(ptr[0]) {
                                    case 0: gudt_resource(name, drv, alt, par, rg, t, rs + rt, rl); break;
                                    case 1: if(ra) gudt_resource(name, drv, alt, par, rg, GUDT_T_IOPORT, rs + rt, rl); break;
                                }
                        }
                    break;
                    case 0x88: /* Word Address Space Descriptor */
                        ra = ((uint64_t)ptr[3] | ((uint64_t)ptr[4] << 8)) + 1;
                        rs = ((uint64_t)ptr[5] | ((uint64_t)ptr[6] << 8));  re = ((uint64_t)ptr[7] | ((uint64_t)ptr[8] << 8));
                        rt = ((uint64_t)ptr[9] | ((uint64_t)ptr[10] << 8)); rl = ((uint64_t)ptr[11] | ((uint64_t)ptr[12] << 8));
                        rg = 1;
                        goto asd;
                    break;
                    case 0x89: /* Extended Interrupt Descriptor */
                        for(n = ptr[1], i = 0; i < n; i++)
                            gudt_resource(name, drv, alt, par, 0, GUDT_T_IRQ, (uint64_t)ptr[2+i*4] | ((uint64_t)ptr[3+i*4] << 8) |
                                ((uint64_t)ptr[4+i*4] << 16) | ((uint64_t)ptr[5+i*4] << 24), 0);
                    break;
                    case 0x8A: /* QWord Address Space Descriptor */
                        memcpy(&ra, ptr + 3, 8); ra++;
                        memcpy(&rs, ptr + 11, 8); memcpy(&re, ptr + 19, 8);
                        memcpy(&rt, ptr + 27, 8); memcpy(&rl, ptr + 35, 8);
                        rg = 3;
                        goto asd;
                    break;
                    case 0x8B: /* Extended Address Space Descriptor */
                        memcpy(&ra, ptr + 5, 8); ra++;
                        memcpy(&rs, ptr + 13, 8); memcpy(&re, ptr + 21, 8);
                        memcpy(&rt, ptr + 29, 8); memcpy(&rl, ptr + 37, 8);
                        rg = ra == 0x100 ? 0 : (ra == 0x10000 ? 1 : (ra ? 2 : 3));
                        goto asd;
                    break;
                    case 0x8C: case 0x8E: /* Connectors */
                    case 0x8D: case 0x8F: case 0x90: case 0x91: case 0x92: /* Pins */
                    case 0x0F: /* End Tag */ break;
                    default: if(verbose) msg("unhandled ResourceTemplate %lx type %02x\r\n",(uintptr_t)ptr, t); break;
                }
                ptr += s;
            }
        } else
        if(aml.node[aml.regidx].type) gudt_resource(name, drv, alt, par, 0, GUDT_T_DEVICE, 0, 0);
    }
}

/**
 * Read AML name
 */
static uint8_t *aml_NameString(uint8_t *ptr)
{
    int c;

#ifdef DEBUG
    if(stkchk + 24 > stkmax) stkmax = stkchk + 24;
#endif
    while(*ptr == '\\' || *ptr == '^') ptr++;
    switch(*ptr) {
        case 0x00: ptr++; c = 0; break;
        case 0x2E: ptr++; c = 8; break;
        case 0x2F: ptr++; c = *ptr++ << 2; break;
        default: c = 4; break;
    }
    return ptr + c;
}

/**
 * Read AML variable
 */
static uint8_t *aml_SuperName(uint8_t *ptr, aml_t *node)
{
    uint8_t *st = ptr;
    uint16_t op;

#ifdef DEBUG
    if(stkchk + 32 > stkmax) stkmax = stkchk + 32;
#endif
    ptr = aml_opcode(ptr, &op);
    node->type = op;
    switch(op) {
        case 0x0000: /* NullStr */      node->data.q[0] = 0; node->type = 0x000D; break;
        case 0x0009: /* Simplename */   if((op = aml_lookup(st))) memcpy(node, &aml.node[op], sizeof(aml_t)); break;
        case 0x0060: case 0x0061: case 0x0062: case 0x0063: case 0x0064: case 0x0065: case 0x0066: case 0x0067: /* LocalX */
        case 0x0068: case 0x0069: case 0x006A: case 0x006B: case 0x006C: case 0x006D: case 0x006E: case 0x006F: /* ArgX */
        break;
        case 0x5B31: /* DebugObj */ break;
        default: ptr = st; break;
    }
    return ptr;
}

/**
 * Read AML field list
 */
static uint8_t *aml_FieldList(uint8_t *ptr, uint8_t *end, aml_t *node)
{
    uint32_t pos = 0;

    AML_STKENTER(24);
    while(ptr < end) {
        switch(*ptr) {
            case 0: ptr++; ptr = aml_pkg(ptr); pos += aml.len; break;
            case 1: ptr += 3; break;
            case 2: ptr = aml_NameString(ptr + 1); break;
            case 3: ptr += 3 + ptr[2]; break;
            default:
                if(aml.num >= aml.max) return (uint8_t*)-1UL;
                NODE.name = ptr; ptr += 4; ptr = aml_pkg(ptr);
                NODE.type = 0x5B13; NODE.parent = aml.par;
                NODE.data.q[0] = node->data.q[0];
                NODE.data.d[2] = pos; NODE.data.d[3] = aml.len;
                pos += aml.len;
                aml.num++;
            break;
        }
    }
    AML_STKLEAVE(24);
    return ptr;
}

/**
 * Interpret AML bytecode
 */
static uint8_t *aml_TermArg(uint8_t *ptr, uint8_t *end, aml_t *node)
{
    aml_t arg1, arg2;
    uint8_t *st = ptr, *s;
    uint16_t op;
    uint32_t l, par, num;

    ptr = aml_opcode(ptr, &op);
    if(ptr > end) return end;
#ifdef DEBUG
    AML_STKENTER(104);
    if(aml.num > numchk) numchk = aml.num;
    if(verbose > 2)
        fprintf(stderr, "%s%08lx op %x (ptr %08lx num %d par %d)\r\n",space(),st-base,op,(uintptr_t)st,aml.num,aml.par);
#endif
    memset(&arg1, 0, sizeof(arg1)); memset(&arg2, 0, sizeof(arg2));
    switch(op) {
        case 0x5B30: /* RevisionOp */
        case 0x0000: /* ZeroOp */       node->data.q[0] = 0; node->type = 0x000E; break;
        case 0x0001: /* OneOp */        node->data.q[0] = 1; node->type = 0x000E; break;
        case 0x00FF: /* OnesOp */       node->data.q[0] = ~0UL; node->type = 0x000E; break;
        case 0x0006: /* AliasOp */
            l = aml_lookup(ptr); ptr = aml_NameString(ptr);
            s = ptr; ptr = aml_NameString(ptr);
            if(l) {
                if(aml.num >= aml.max) return (uint8_t*)-1UL;
                memcpy(&NODE, &aml.node[l], sizeof(aml_t));
                NODE.name = s; NODE.parent = aml.par; aml_fn(&NODE);
                aml.num++;
            }
        break;
        case 0x0008: /* NameOp */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = par = aml.par; aml.par = aml.num; aml_fn(&NODE);
            if(!memcmp(NODE.name, "_HID", 4)) aml.drvidx = aml.num; else
            if(!memcmp(NODE.name, "_CID", 4)) aml.altidx = aml.num; else
            if(!memcmp(NODE.name, "_CRS", 4)) aml.regidx = aml.num;
            num = aml.num++; ptr = aml_TermArg(ptr, end, &aml.node[num]);
            aml.num = num + 1; aml.par = par;
        break;
        case 0x0009: /* ObjReference/Call */
            if((l = aml_lookup(st))) {
                if(aml.node[l].type == 0x0014) {
                    if(aml.sp >= 16 && aml.sp - 16 < AML_MAXLEVEL*16) {
                        aml.sp -= 8;
                        for(par = 0; par <= (aml.node[l].data.d[3] & 7); par++) {
                            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
                            aml.stack[aml.sp + par] = arg1.data.q[0];
                        }
                        aml.sp -= 8;
#ifdef DEBUG
                        if(verbose > 2)
                            fprintf(stderr,"%sMETHOD '%c%c%c%c' %08lx %08lx\n", space(), aml.node[l].name[0], aml.node[l].name[1],
                                aml.node[l].name[2], aml.node[l].name[3],
                                aml.node[l].data.p[0] - base, aml.node[l].data.p[0] + aml.node[l].data.d[2] - base);
                        lvl++;
#endif
                        aml_TermList(aml.node[l].data.p[0], aml.node[l].data.p[0] + aml.node[l].data.d[2], &arg1);
#ifdef DEBUG
                        lvl--;
                        if(verbose > 2) fprintf(stderr,"%sMETHOD END %08lx\n", space(), ptr - base);
#endif
                        node->data.q[0] = arg1.data.q[0]; node->data.q[1] = arg1.data.q[1]; node->type = arg1.type;
                    }
                } else { memcpy(&node->data, &aml.node[l].data, sizeof(arg1.data)); node->type = aml.node[l].type; }
            }
        break;
        case 0x000A: /* BytePrefix */   node->data.q[0] = *ptr++; node->type = op; break;
        case 0x000B: /* WordPrefix */   node->data.q[0] = 0; memcpy(node->data.q, ptr, 2); ptr += 2; node->type = op; break;
        case 0x000C: /* DWordPrefix */  node->data.q[0] = 0; memcpy(node->data.q, ptr, 4); ptr += 4; node->type = op; break;
        case 0x000E: /* QWordPrefix */  node->data.q[0] = 0; memcpy(node->data.q, ptr, 8); ptr += 8; node->type = op; break;
        case 0x000D: /* StringPrefix */ node->data.p[0] = ptr; node->type = op; while(ptr[-1]) ptr++; break;
        case 0x0010: /* Scope */
        case 0x0014: /* Method */
        case 0x5B82: /* Device */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_pkg(ptr); s = st + (op > 0xff ? 2 : 1) + aml.len;
            if(op == 0x5B82) aml.devidx = aml.num;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = par = aml.par; aml.par = aml.num;
            aml_fn(&NODE);
            if(op == 0x0014) {
                NODE.data.d[3] = (*ptr++ & 7);
                NODE.data.p[0] = ptr;
                NODE.data.d[2] = s - ptr;
                l = 0; aml.mtdidx = aml.num;
                if(!memcmp(NODE.name, "_HID", 4)) { l = 1; aml.drvidx = aml.num; } else
                if(!memcmp(NODE.name, "_CID", 4)) { l = 1; aml.altidx = aml.num; } else
                if(!memcmp(NODE.name, "_CRS", 4)) { l = 1; aml.regidx = aml.num; }
                if(l) { if(!aml.sp) l = 0; else { aml.sp -= 16; memset(&aml.stack[aml.sp], 0, 16 * sizeof(aml.stack[0])); } }
            } else l = 1;
#ifdef DEBUG
            if(verbose > 1)
                fprintf(stderr, "%sSCOPE++   %04x   '%c%c%c%c'            num %d par %d aml.len %d s %lx l %d sp %d\r\n", space(),
                    op, NODE.name[0],NODE.name[1],NODE.name[2],NODE.name[3],aml.num,par,aml.len,s - base,l,aml.sp);
            lvl++;
#endif
            num = aml.num++;
            if(l) ptr = aml_TermList(ptr, s, &aml.node[num]);
#ifdef DEBUG
            else if(verbose > 2) fprintf(stderr, "%sskip to %08lx\r\n", space(), s - base);
#endif
            ptr = s;
            if(op == 0x0014) { aml.mtdidx = -1U; if(l) aml.sp += 16; }
#ifdef DEBUG
            lvl--;
            if(verbose > 1)
                fprintf(stderr, "%sSCOPE-- num %d (new %d) %08lx sp %d\r\n", space(), aml.num, num+1, ptr-base, aml.sp);
#endif
            if(op == 0x5B82 && aml.devidx != -1U && aml.drvidx != -1U && aml.regidx != -1U) {
                aml_ResourceTemplate();
                aml.devidx = aml.drvidx = aml.altidx = aml.regidx = -1U;
            }
            aml.num = num + 1; aml.par = par;
        break;
        case 0x0015: /* ExternalOp */
            ptr = aml_NameString(ptr) + 2;
        break;
        case 0x0011: /* Buffer */
            ptr = aml_pkg(ptr); st += 1 + aml.len;
            ptr = aml_TermArg(ptr, end, &arg1);
            node->data.q[1] = arg1.data.q[0];
            node->data.p[0] = ptr;
            node->type = op;
            ptr = st;
        break;
        case 0x0012: /* Package */
            ptr = aml_pkg(ptr); st += 1 + aml.len;
            node->data.d[2] = aml.len;
            node->data.d[3] = num = *ptr++;
            node->data.p[0] = ptr;
            node->type = op;
            if(aml.num && aml.node[aml.num-1].name && aml.node[aml.num-1].name[0] == '_' &&
              aml.node[aml.num-1].name[1] == 'S' && aml.node[aml.num-1].name[2] >= '3' && aml.node[aml.num-1].name[2] <= '5' &&
              aml.node[aml.num-1].name[3] == '_') {
                arg1.data.w[0] = aml.node[aml.num-1].name[2];
                if(num > 4) num = 4;
                for(l = 0; l < num; l++)
                    switch(ptr[0]) {
                        case 0: case 1: case 0xff: arg1.data.w[l + 1] = *ptr++; break;
                        case 0xA: ptr++; arg1.data.w[l + 1] = *ptr++; break;
                        case 0xB: ptr++; arg1.data.w[l + 1] = ptr[0] | (ptr[1] << 8); ptr += 2; break;
                        case 0xC: ptr++; arg1.data.w[l + 1] = 0; ptr += 4; break;
                        case 0xD: ptr++; arg1.data.w[l + 1] = 0; ptr += 8; break;
                    }
                gudt_resource("PM1aC", "PNP0C23", "", 0, ((num + 1) << 4) | 1, GUDT_T_DEFAULT, (uintptr_t)arg1.data.w, num + 1);
            }
            ptr = st;
        break;
        case 0x0013: /* VarPackage */
            ptr = aml_pkg(ptr); st += 1 + aml.len;
            ptr = aml_TermArg(ptr, end, node);
            node->data.d[2] = aml.len;
            node->data.d[3] = 0;
            node->data.p[0] = ptr;
            node->type = op;
            ptr = st;
        break;
        case 0x5B01: /* MutexOp */      ptr = aml_NameString(ptr) + 1; break;
        case 0x5B02: /* EventOp */      ptr = aml_NameString(ptr); break;
        case 0x5B12: /* CondRefOfOp */
        case 0x0071: /* RefOfOp */
            ptr = aml_SuperName(ptr, &arg1);
            ptr = aml_SuperName(ptr, &arg2);
            if(arg1.type != 0x0009) {
                aml_set(&arg2, &arg1);
                memcpy(node, &arg1, sizeof(arg1));
            } else
            if(op == 0x0071) return (uint8_t*)-1UL;
        break;
        case 0x5B1F: /* LoadTableOp */
            ptr = aml_TermArg(ptr, end, &arg1); ptr = aml_TermArg(ptr, end, &arg1); ptr = aml_TermArg(ptr, end, &arg1);
            ptr = aml_TermArg(ptr, end, &arg1); ptr = aml_TermArg(ptr, end, &arg1); ptr = aml_TermArg(ptr, end, &arg1);
        break;
        case 0x5B20: /* LoadOp */
            l = aml_lookup(ptr); ptr = aml_NameString(ptr);
            ptr = aml_SuperName(ptr, &arg1);
            if(l) { memcpy(&aml.node[l].data, &arg1.data, sizeof(arg1.data)); aml.node[l].type = arg1.type; }
        break;
        case 0x5B21: /* StallOp */
        case 0x5B22: /* SleepOp */
            ptr = aml_TermArg(ptr, end, &arg1);
        break;
        case 0x5B23: /* AcquireOp */
            ptr = aml_SuperName(ptr, &arg1) + 2;
        break;
        case 0x5B24: /* SignalOp */
        case 0x5B26: /* ResetOp */
        case 0x5B27: /* ReleaseOp */
            ptr = aml_SuperName(ptr, &arg1);
        break;
        case 0x5B25: /* WaitOp */
            ptr = aml_SuperName(ptr, &arg1);
            ptr = aml_TermArg(ptr, end, &arg1);
        break;
        case 0x5B28: /* FromBCDOp */
        case 0x5B29: /* ToBCDOp */
            ptr = aml_TermArg(ptr, end, &arg1);
            ptr = aml_SuperName(ptr, &arg1);
        break;
        case 0x5B32: /* FatalOp */ return (uint8_t*)-1UL;
        case 0x5B80: /* OpRegion */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = aml.par; aml_fn(&NODE);
            NODE.data.b[12] = *ptr++;           /* space */
            ptr = aml_TermArg(ptr, end, &arg1); /* address */
            NODE.data.q[0] = arg1.data.q[0];
            ptr = aml_TermArg(ptr, end, &arg1); /* length */
            NODE.data.d[2] = arg1.data.d[0];
            if(!NODE.data.b[12] && NODE.data.q[0]) mem_add(GUDT_T_RESVMEM, NODE.data.q[0], NODE.data.d[2]);
            aml.num++;
        break;
        case 0x5B81: /* Field */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_pkg(ptr); st += 2 + aml.len;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = aml.par; aml_fn(&NODE);
            NODE.data.d[0] = 0;
            NODE.data.d[1] = *ptr++;        /* flags */
            aml.num++; ptr = aml_FieldList(ptr, st, &aml.node[aml.num - 1]);
            ptr = st;
        break;
        case 0x5B83: /* Processor (nem használt a v6.4 óta) */
            ptr = aml_pkg(ptr);
            arg1.data.q[0] = 0; memcpy(&arg1.data, ptr, 4);
            gudt_resource((char*)&arg1.data, "CORE", "", 0, 0, GUDT_T_CPUCORE, 0, ptr[4]);
            ptr = st + 2 + aml.len;
        break;
        case 0x5B86: /* IndexField */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_pkg(ptr); st += 2 + aml.len;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = aml.par; aml_fn(&NODE);
            NODE.data.d[0] = aml_lookup(ptr); ptr = aml_NameString(ptr);
            NODE.data.d[1] = *ptr++;         /* flags */
            aml.num++; ptr = aml_FieldList(ptr, st, &aml.node[aml.num - 1]);
            ptr = st;
        break;
        case 0x5B87: /* BankField */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_pkg(ptr); st += 2 + aml.len;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = aml.par; aml_fn(&NODE);
            NODE.data.d[0] = aml_lookup(ptr); ptr = aml_NameString(ptr);
            ptr = aml_TermArg(ptr, end, &arg1);
            NODE.data.d[1] = *ptr++;         /* flags */
            NODE.data.q[1] = arg1.data.q[0];
            aml.num++; ptr = aml_FieldList(ptr, st, &aml.node[aml.num - 1]);
            ptr = st;
        break;
        case 0x5B88: /* DataRegion */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_pkg(ptr); st += 2 + aml.len;
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = op; NODE.parent = aml.par; aml_fn(&NODE);
            ptr = aml_TermArg(ptr, end, &arg1); NODE.data.q[0] = arg1.data.q[0];
            ptr = aml_TermArg(ptr, end, &arg1); NODE.data.d[2] = arg1.data.d[0];
            ptr = aml_TermArg(ptr, end, &arg1); NODE.data.d[3] = arg1.data.d[0];
            aml.num++;
            ptr = st;
        break;
        case 0x0060: case 0x0061: case 0x0062: case 0x0063: case 0x0064: case 0x0065: case 0x0066: case 0x0067: /* LocalX */
        case 0x0068: case 0x0069: case 0x006A: case 0x006B: case 0x006C: case 0x006D: case 0x006E: case 0x006F: /* ArgX */
            node->type = op; op = aml.sp + op - 0x60; node->data.q[0] = op < AML_MAXLEVEL*16 ? aml.stack[op] : 0;
        break;
        case 0x0070: /* StoreOp */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
            ptr = aml_SuperName(ptr, &arg2); aml_set(&arg2, &arg1);
        break;
        case 0x0075: /* IncrementOp */
            ptr = aml_SuperName(ptr, &arg1); aml_get(&arg2, &arg1);
            arg2.data.q[0]++; aml_set(&arg1, &arg2);
            node->data.q[0] = arg2.data.q[0]; node->type = arg2.type;
        break;
        case 0x0076: /* DecrementOp */
            ptr = aml_SuperName(ptr, &arg1); aml_get(&arg2, &arg1);
            arg2.data.q[0]--; aml_set(&arg1, &arg2);
            node->data.q[0] = arg2.data.q[0]; node->type = arg2.type;
        break;
        case 0x0083: /* DerefOfOp */
            ptr = aml_TermArg(ptr, end, &arg1);
            if(arg1.type == 0x000D) {
                l = aml_lookup(arg1.data.p[0]);
                if(l) memcpy(&arg1, &aml.node[l], sizeof(arg1));
            }
            memcpy(node, &arg1, sizeof(arg1));
        break;
        case 0x0072: /* AddOp */
        case 0x0073: /* ConcatOp */
        case 0x0074: /* SubtractOp */
        case 0x0077: /* MultiplyOp */
        case 0x0078: /* DivideOp */
        case 0x0079: /* ShiftLeftOp */
        case 0x007A: /* ShiftRightOp */
        case 0x007B: /* AndOp */
        case 0x007C: /* NandOp */
        case 0x007D: /* OrOp */
        case 0x007E: /* NorOp */
        case 0x007F: /* XorOp */
        case 0x0084: /* ConcatResOp */
        case 0x0085: /* ModOp */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
            ptr = aml_TermArg(ptr, end, &arg2); aml_get(&arg2, &arg2);
            switch(op) {
                case 0x0072: arg1.data.q[0] += arg2.data.q[0]; break;
                case 0x0074: arg1.data.q[0] -= arg2.data.q[0]; break;
                case 0x0077: arg1.data.q[0] *= arg2.data.q[0]; break;
                case 0x0078: arg1.data.q[0] = arg2.data.q[0] ? arg1.data.q[0] / arg2.data.q[0] : 0; break;
                case 0x0079: arg1.data.q[0] <<= arg2.data.q[0]; break;
                case 0x007A: arg1.data.q[0] >>= arg2.data.q[0]; break;
                case 0x007B: arg1.data.q[0] &= arg2.data.q[0]; break;
                case 0x007C: arg1.data.q[0] = ~(arg1.data.q[0] & arg2.data.q[0]); break;
                case 0x007D: arg1.data.q[0] |= arg2.data.q[0]; break;
                case 0x007E: arg1.data.q[0] = ~(arg1.data.q[0] | arg2.data.q[0]); break;
                case 0x007F: arg1.data.q[0] ^= arg2.data.q[0]; break;
                case 0x0085: arg1.data.q[0] = arg2.data.q[0] ? arg1.data.q[0] % arg2.data.q[0] : 0; break;
            }
            ptr = aml_SuperName(ptr, &arg2); aml_set(&arg2, &arg1);
            node->data.q[0] = arg1.data.q[0]; node->type = arg1.type;
        break;
        case 0x0088: /* IndexOp */
            ptr = aml_TermArg(ptr, end, &arg1);
            ptr = aml_TermArg(ptr, end, &arg2); aml_get(&arg2, &arg2);
            arg1.data.q[0] += arg2.data.q[0];
            ptr = aml_SuperName(ptr, &arg2); aml_set(&arg2, &arg1);
            node->data.q[0] = arg1.data.q[0]; node->type = arg1.type;
        break;
        case 0x0090: /* LandOp */
        case 0x0091: /* LorOp */
        case 0x9293: /* LNotEqualOp */
        case 0x9294: /* LLessEqualOp */
        case 0x9295: /* LGreaterEqualOp */
        case 0x0093: /* LEqualOp */
        case 0x0094: /* LGreaterOp */
        case 0x0095: /* LLessOp */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
            ptr = aml_TermArg(ptr, end, &arg2); aml_get(&arg2, &arg2);
            switch(op) {
                case 0x0090: arg1.data.q[0] = (arg1.data.q[0] && arg2.data.q[0]); break;
                case 0x0091: arg1.data.q[0] = (arg1.data.q[0] || arg2.data.q[0]); break;
                case 0x9293: arg1.data.q[0] = (arg1.data.q[0] != arg2.data.q[0]); break;
                case 0x9294: arg1.data.q[0] = (arg1.data.q[0] <= arg2.data.q[0]); break;
                case 0x9295: arg1.data.q[0] = (arg1.data.q[0] >= arg2.data.q[0]); break;
                case 0x0093: arg1.data.q[0] = (arg1.data.q[0] == arg2.data.q[0]); break;
                case 0x0094: arg1.data.q[0] = (arg1.data.q[0] > arg2.data.q[0]); break;
                case 0x0095: arg1.data.q[0] = (arg1.data.q[0] < arg2.data.q[0]); break;
            }
            node->data.q[0] = arg1.data.q[0]; node->type = arg1.type;
        break;
        case 0x0081: /* FindSetLeftBitOp */
        case 0x0082: /* FindSetRightBitOp */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
            ptr = aml_SuperName(ptr, &arg2);
            if(arg1.data.q[0]) {
                if(op == 0x0081) for(l = 1; arg1.data.q[0] & 1; l++, arg1.data.q[0] >>= 1);
                else for(l = 1; !(arg1.data.q[0] & 1); l++, arg1.data.q[0] >>= 1);
            } else l = 0;
            arg2.data.q[0] = l; aml_set(&arg2, &arg2);
            node->data.q[0] = arg2.data.q[0]; node->type = arg2.type;
        break;
        case 0x0092: /* LnotOp */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);
            arg1.data.q[0] = !arg1.data.q[0];
            ptr = aml_SuperName(ptr, &arg2); aml_set(&arg2, &arg1);
            node->data.q[0] = arg2.data.q[0]; node->type = arg2.type;
        break;
        case 0x0086: /* NotifyOp */
            ptr = aml_SuperName(ptr, &arg1);
            ptr = aml_TermArg(ptr, end, &arg2);
#ifdef DEBUG
            if(verbose > 2) {
                aml_get(&arg2, &arg2);
                fprintf(stderr, "NotifyOp\r\n");
                aml_dump(&arg1);
                aml_dump(&arg2);
            }
#endif
        break;
        case 0x0087: /* SizeofOp */
            ptr = aml_SuperName(ptr, &arg1);
            node->data.q[0] = 0;
            switch(arg1.type) {
                case 0x000A: node->data.q[0] = 1; break;
                case 0x000B: node->data.q[0] = 2; break;
                case 0x000C: node->data.q[0] = 4; break;
                case 0x000D: if((s = node->data.p[0]) && s >= aml.memmin && s < aml.memmax) for(; *s; s++, node->data.q[0]++); break;
                case 0x0060: case 0x0061: case 0x0062: case 0x0063: case 0x0064: case 0x0065: case 0x0066: case 0x0067:
                case 0x0068: case 0x0069: case 0x006A: case 0x006B: case 0x006C: case 0x006D: case 0x006E: case 0x006F:
                case 0x000E: node->data.q[0] = 8; break;
                case 0x0011: case 0x0012: case 0x0013: case 0x5B80: node->data.q[0] = arg1.data.d[2]; break;
                case 0x5B13: node->data.q[0] = (arg1.data.d[3] + 7) >> 3; break;
            }
            node->type = 0x000E;
        break;
        case 0x0089: /* MatchOp */
            ptr = aml_TermArg(ptr, end, &arg1);                         /* package */
            ptr++;                                                      /* op1 */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);  /* obj1 */
            ptr++;                                                      /* op2 */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);  /* obj2 */
            ptr = aml_TermArg(ptr, end, &arg1); aml_get(&arg1, &arg1);  /* startindex */
            node->data.q[0] = ~0UL; node->type = 0x000E;
        break;
        case 0x008A: /* CreateDWordField */
        case 0x008B: /* CreateWordField */
        case 0x008C: /* CreateByteField */
        case 0x008D: /* CreateBitField */
        case 0x008F: /* CreateQWordField */
        case 0x5B13: /* CreateFieldOp */
            if(aml.num >= aml.max) return (uint8_t*)-1UL;
            ptr = aml_TermArg(ptr, end, &arg1);          /* SourceBuff */
            ptr = aml_TermArg(ptr, end, &arg2);          /* ByteIndex (0x8D/0x5B13=BitIndex) */
            NODE.name = ptr; ptr = aml_NameString(ptr);
            NODE.type = 0x5B13; NODE.parent = aml.par; aml_fn(&NODE);
            NODE.data.q[0] = arg1.data.q[0]; NODE.data.d[2] = arg2.data.d[0];
            switch(op) {                            /* size */
                case 0x008A: NODE.data.d[3] = 32; NODE.data.d[2] <<= 3; break;
                case 0x008B: NODE.data.d[3] = 16; NODE.data.d[2] <<= 3; break;
                case 0x008C: NODE.data.d[3] = 8; NODE.data.d[2] <<= 3; break;
                case 0x008D: NODE.data.d[3] = 1; break;
                case 0x008F: NODE.data.d[3] = 64; NODE.data.d[2] <<= 3; break;
                case 0x5B13: ptr = aml_TermArg(ptr, end, &arg2); NODE.data.d[3] = arg2.data.d[0]; break;
            }
            aml.num++;
        break;
        case 0x008E: /* ObjectType */
            ptr = aml_SuperName(ptr, &arg1);
            node->type = 0x000E;
            switch(arg1.type) {
                case 0x000A: case 0x000B: case 0x000C: case 0x000E: node->data.q[0] = 1; break; /* Integer */
                case 0x000D: node->data.q[0] = 2; break;                                        /* String */
                case 0x0011: node->data.q[0] = 3; break;                                        /* Buffer */
                case 0x0012: case 0x0013: node->data.q[0] = 4; break;                           /* Package */
                case 0x5B81: node->data.q[0] = 5; break;                                        /* Field */
                case 0x5B82: node->data.q[0] = 6; break;                                        /* Device */
                case 0x5B02: node->data.q[0] = 7; break;                                        /* Event */
                case 0x0014: node->data.q[0] = 8; break;                                        /* Method */
                case 0x5B01: node->data.q[0] = 9; break;                                        /* Mutex */
                case 0x5B80: node->data.q[0] =10; break;                                        /* OpRegion */
                case 0x5B84: node->data.q[0] =11; break;                                        /* PowerRes */
                case 0x5B83: node->data.q[0] =12; break;                                        /* Processor */
                case 0x5B85: node->data.q[0] =13; break;                                        /* ThermalZone */
                case 0x5B13: node->data.q[0] =14; break;                                        /* BufferField */
                case 0x5B1F: case 0x5B20: node->data.q[0] =15; break;                           /* DDBHandle */
                case 0x5B30: node->data.q[0] =16; break;                                        /* DebugObj */
                default: node->data.q[0] = 0;
            }
        break;
        case 0x00A0: /* If */
            ptr = aml_pkg(ptr); st += 1 + aml.len;
            ptr = aml_TermArg(ptr, end, &arg1);
            if(arg1.data.q[0]) ptr = aml_TermList(ptr, st, node); else ptr = st;
            if(*ptr == 0xA1) {
                ptr = aml_pkg(ptr + 1); st += 1 + aml.len;
                if(arg1.data.q[0]) ptr = st; else ptr = aml_TermList(ptr, st, node);
            }
        break;
        case 0x00A1: /* Else blokk If nélkül? */
            ptr = aml_pkg(ptr); ptr = st + 1 + aml.len;
        break;
        case 0x00A2: /* While */
            ptr = aml_pkg(ptr); ptr = st + 1 + aml.len;
        break;
        case 0x00A4: /* Return */
            ptr = aml_TermArg(ptr, end, &arg1);
            memcpy(&node->data, &arg1.data, sizeof(arg1.data)); node->type = arg1.type;
        break;
        case 0x5B2A: /* reserved */
        case 0x5B33: /* TimerOp */
        case 0x009F: /* Continue */
        case 0x00A3: /* Noop */
        case 0x00CC: /* BreakPoint */ break;
        default: ptr = st; break;
    }
    AML_STKLEAVE(104);
    return ptr;
}

/**
 * Interpret AML bytecode term list
 */
static uint8_t *aml_TermList(uint8_t *ptr, uint8_t *end, aml_t *node)
{
    uint8_t *st;

    AML_STKENTER(24);
    if(!node) { node = &aml.node[0]; memset(node, 0, sizeof(aml_t)); }
#ifdef DEBUG
    if(verbose > 2) fprintf(stderr,"%sTermList in %lx %lx\n",space(),ptr-base,end-base);
    lvl++;
#endif
    while(ptr < end) {
        st = ptr; ptr = aml_TermArg(st, end, node);
        if(ptr == st) {
#ifdef DEBUG
            if(verbose > 2) fprintf(stderr, "%sunknown opcode %08lx %02x %02x\r\n", space(), ptr - base, ptr[0], ptr[1]);
#endif
            return (uint8_t*)-1UL;
        }
    }
#ifdef DEBUG
    lvl--;
    if(verbose > 2) fprintf(stderr,"%sTermList out %lx %lx\n",space(),ptr-base,end-base);
#endif
    AML_STKLEAVE(24);
    return ptr;
}

/**
 * Add a GAS resource
 */
void aml_gas(char *name, char *driver, gas_t *gas)
{
    uint32_t t = gas->type ? gas->type - 1 : 0;
    if(gas->address && gas->space < 32)
        gudt_resource(name, driver, "", 0, t, GUDT_T_MMIO + gas->space, gas->address, 1 << t);
}

/**
 * Parse AML bytecode
 */
void aml_parse(uint8_t *ptr, char *fn)
{
    fadt_t *fadt;
    uint8_t *end;
    uint64_t lapic = 0;
    char brand[9] = { 0 }, model[9] = { 0 };
    int n;

    if(!memcmp(ptr, "APIC", 4)) {
        if(!gudt_hdr.hdr.numnodes) {
err:        msg("%s: specify an DSDT input first\r\n", fn);
            return;
        }
        lapic = ((madt_t*)ptr)->lapic_addr;
        for(end = ptr + ((acpi_t*)ptr)->size, ptr += sizeof(madt_t); ptr < end && ptr[1]; ptr += ptr[1]) {
            switch(*ptr) {
                case 0:
                    if(((madt_cpu_t*)ptr)->flags)
                        gudt_resource("C000", "CORE", "", 0, 0, GUDT_T_CPUCORE, 0, ((madt_cpu_t*)ptr)->apic_id);
                break;
                case 1:
                    gudt_resource("IOAPIC", "PNP0C08", "", 0, 0, GUDT_T_MMIO, ((madt_ioapic_t*)ptr)->address, 256);
                    gudt_resource("IOAPIC", "PNP0C08", "", 0, 0, GUDT_T_IRQ, ((madt_ioapic_t*)ptr)->int_base, 0);
                break;
                case 5: lapic = ((madt_lapic_t*)ptr)->address; break;
                case 9:
                    if(((madt_cpu2_t*)ptr)->flags)
                        gudt_resource("C000", "PNP0003", "", 0, 0, GUDT_T_CPUCORE, 0, ((madt_cpu2_t*)ptr)->apic_id);
                break;
            }
        }
        if(lapic) gudt_resource("C000", "CORE", "", 0, 0, GUDT_T_MMIO, lapic, 1024);
    } else
    if(!memcmp(ptr, "FACP", 4)) {
        if(!gudt_hdr.hdr.numnodes) goto err;
        fadt = (fadt_t*)ptr;
        ((gudt_device_t*)gudt_nodes)->device = fadt->preferred_pm_profile;
        gudt_resource("SMI_CMD", "PNP0C20", "", 0, 0, GUDT_T_IRQ, fadt->sci_int, 0);
        gudt_resource("SMI_CMD", "PNP0C20", "", 0, 0, GUDT_T_IOPORT, fadt->smi_cmd, 1);
        gudt_resource("SMI_CMD", "PNP0C20", "", 0, 0x20, GUDT_T_DEFAULT, (uintptr_t)&fadt->acpi_enable, 2);
        if(fadt->hdr.rev >= 2 && fadt->hdr.size >= sizeof(fadt_t)) {
            aml_gas("PM1aE", "PNP0C21", &fadt->x_pm1a_evt_blk);
            aml_gas("PM1bE", "PNP0C22", &fadt->x_pm1b_evt_blk);
            aml_gas("PM1aC", "PNP0C23", &fadt->x_pm1a_cnt_blk);
            aml_gas("PM1bC", "PNP0C24", &fadt->x_pm1b_cnt_blk);
            aml_gas("PM2C",  "PNP0C25", &fadt->x_pm2_cnt_blk);
            aml_gas("PMTMR", "PNP0C26", &fadt->x_pm_tmr_blk);
            aml_gas("GPE0",  "PNP0C27", &fadt->x_gpe0_blk);
            aml_gas("GPE1",  "PNP0C28", &fadt->x_gpe1_blk);
        } else {
            gudt_resource("PM1aE", "PNP0C21", "", 0, 0, GUDT_T_IOPORT, fadt->pm1a_evt_blk, fadt->pm1_evt_len);
            gudt_resource("PM1bE", "PNP0C22", "", 0, 0, GUDT_T_IOPORT, fadt->pm1b_evt_blk, fadt->pm1_evt_len);
            gudt_resource("PM1aC", "PNP0C23", "", 0, 0, GUDT_T_IOPORT, fadt->pm1a_cnt_blk, fadt->pm1_cnt_len);
            gudt_resource("PM1bC", "PNP0C24", "", 0, 0, GUDT_T_IOPORT, fadt->pm1b_cnt_blk, fadt->pm1_cnt_len);
            gudt_resource("PM2C",  "PNP0C25", "", 0, 0, GUDT_T_IOPORT, fadt->pm2_cnt_blk, fadt->pm2_cnt_len);
            gudt_resource("PMTMR", "PNP0C26", "", 0, 0, GUDT_T_IOPORT, fadt->pm_tmr_blk, fadt->pm_tmr_len);
            gudt_resource("GPE0",  "PNP0C27", "", 0, 0, GUDT_T_IOPORT, fadt->gpe0_blk, fadt->gpe0_blk_len);
            gudt_resource("GPE1",  "PNP0C28", "", 0, 0, GUDT_T_IOPORT, fadt->gpe1_blk, fadt->gpe1_blk_len);
        }
    } else
    if((ptr[0] == 'D' || ptr[0] == 'S') && !memcmp(ptr + 1, "SDT", 3) && ((acpi_t*)ptr)->size) {
        memcpy(brand, ptr + 10, 6); memcpy(model, ptr + 16, 8);
        gudt_resource(model, brand, "", 0, 0, -1, 0, 0);
        if((n = aml_numnodes(1, (acpi_t**)&ptr)) > 0) {
            aml.node = __builtin_alloca(n * sizeof(aml_t));
            memset(aml.node, 0, n * sizeof(aml_t));
#ifdef DEBUG
            base = ptr;
#endif
            aml_TermList(ptr + sizeof(acpi_t), ptr + ((acpi_t*)ptr)->size, NULL);
        }
    } else {
        msg("%s: not a valid AML\r\n", fn);
        return;
    }
}
